﻿using System;
using System.Reflection;

namespace vJine.Core {
    public delegate object InvokeSignature(object objContext, params object[] Params);

    public delegate Tobj InitSignature<Tobj>(params object[] Params);
    public delegate void SetSignature<Tobj, Tprop>(Tobj ObjContext, Tprop V);
    public delegate Tprop GetSignature<Tobj, Tprop>(Tobj objContext);
    public delegate void CallSignature<Tobj>(Tobj objContext, params object[] Params);
    public delegate TReturn CallSignature<Tobj, TReturn>(Tobj objContext, params object[] Params);

    #region Exec & Call

    public delegate void Exec();

    public delegate void Exec<T1>(T1 arg1);

    public delegate void Exec<T1, T2>(T1 arg1, T2 arg2);

    public delegate void Exec<T1, T2, T3>(T1 arg1, T2 arg2, T3 arg3);

    public delegate void Exec<T1, T2, T3, T4>(T1 arg1, T2 arg2, T3 arg3, T4 arg4);

    public delegate void Exec<T1, T2, T3, T4, T5>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5);

    public delegate void Exec<T1, T2, T3, T4, T5, T6>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6);

    public delegate void Exec<T1, T2, T3, T4, T5, T6, T7>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7);

    public delegate void Exec<T1, T2, T3, T4, T5, T6, T7, T8>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8);

    public delegate void Exec<T1, T2, T3, T4, T5, T6, T7, T8, T9>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9);
    public delegate R Call<R>();

    public delegate R Call<R, T1>(T1 arg1);

    public delegate R Call<R, T1, T2>(T1 arg1, T2 arg2);

    public delegate R Call<R, T1, T2, T3>(T1 arg1, T2 arg2, T3 arg3);

    public delegate R Call<R, T1, T2, T3, T4>(T1 arg1, T2 arg2, T3 arg3, T4 arg4);

    public delegate R Call<R, T1, T2, T3, T4, T5>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5);

    public delegate R Call<R, T1, T2, T3, T4, T5, T6>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6);

    public delegate R Call<R, T1, T2, T3, T4, T5, T6, T7>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7);

    public delegate R Call<R, T1, T2, T3, T4, T5, T6, T7, T8>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8);

    public delegate R Call<R, T1, T2, T3, T4, T5, T6, T7, T8, T9>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9);
    #endregion Exec & Call
}
