﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using vJine.Core.IoC;
using System.Reflection;
using System.Collections;
using vJine.Core.ORM;

namespace vJine.Core.IO.Json {
    /// <summary>
    /// Json序列化、反序列化帮助类
    /// </summary>
    public partial class JsonHelper {
        /// <summary>
        /// 实例化帮助类
        /// </summary>
        public JsonHelper() {
        }
        #region ToString
        internal static readonly string NULL = "null";
        /// <summary>
        /// 序列化实体类实例为Json字符串
        /// </summary>
        /// <param name="entity">实体类实例</param>
        /// <returns>Json字符串</returns>
        public static string ToString(object entity) {
            MemoryStream mm = new MemoryStream();
            StreamWriter jsonWriter = new StreamWriter(mm, Encoding.UTF8);
            JsonHelperCache.ToString(entity.GetType())(entity, jsonWriter);
            jsonWriter.Flush();
            string json = Encoding.UTF8.GetString(mm.ToArray());
            mm.Close(); mm = null;
            return json;
        }
        /// <summary>
        /// 序列化实体类实例到流
        /// </summary>
        /// <param name="entity">实体类实例</param>
        /// <param name="jsonStream">流</param>
        /// <returns>写入字节长度(未实现)</returns>
        public static int ToString(object entity, Stream jsonStream) {
            StreamWriter jsonWriter = new StreamWriter(jsonStream, Encoding.UTF8);
            JsonHelperCache.ToString(entity.GetType())(entity, jsonWriter);
            jsonWriter.Flush();

            return -1;
        }

        /// <summary>
        /// 序列化实体类实例为Json字符串
        /// </summary>
        /// <typeparam name="Tentity">实体类型</typeparam>
        /// <param name="entity">实体类实例</param>
        /// <returns>Json字符串</returns>
        public static string ToString<Tentity>(Tentity entity) {
            return JsonHelper<Tentity>.ToString(entity);
        }
        /// <summary>
        /// 序列化实体类实例到流
        /// </summary>
        /// <typeparam name="Tentity">实体类型</typeparam>
        /// <param name="entity">实体实例</param>
        /// <param name="jsonStream">流</param>
        public static void ToString<Tentity>(Tentity entity, Stream jsonStream) {
            JsonHelper<Tentity>.ToString(entity, jsonStream);
        }

        #endregion ToString

        #region Parse
        /// <summary>
        /// 反序列化Json字符串为实体类实例
        /// </summary>
        /// <param name="json">Json字符串</param>
        /// <param name="entity">实体类实例</param>
        public static object Parse(string json, object entity) {
            return JsonHelper.Parse(json, entity.GetType());
        }
        /// <summary>
        /// 反序列化字节数组为实体类实例
        /// </summary>
        /// <param name="json">字节数组</param>
        /// <param name="entity">实体类实例</param>
        public static void Parse(byte[] json, object entity) {
            MemoryStream jsonStream = new MemoryStream(json);
            jsonStream.Position = 0;

            parseObject.MakeGenericMethod(entity.GetType())
                .Invoke(null, new object[] { jsonStream, new byte[6], entity });
        }
        /// <summary>
        /// 反序列化Json字符串为指定类型的实体实例
        /// </summary>
        /// <param name="json">Json字符串</param>
        /// <param name="entityType">实体类型</param>
        /// <returns>实体实例</returns>
        public static object Parse(string json, Type entityType) {
            MemoryStream jsonStream = new MemoryStream(Encoding.UTF8.GetBytes(json));
            jsonStream.Position = 0;

            object jsonObject = null;
            if (Reflect.IsArray(entityType)) {
                Type tBase = entityType.GetElementType();
                Type tList = typeof(List<>).MakeGenericType(tBase);
                MethodInfo mToArray = tList.GetMethod("ToArray");
                jsonObject = Class.Create(tList);
                parseList.MakeGenericMethod(tBase)
                .Invoke(null, new object[] { jsonStream, new byte[6], jsonObject });
                return mToArray.Invoke(jsonObject, null);

            } else {
                jsonObject = Class.Create(entityType);
                return parseObject.MakeGenericMethod(entityType)
                .Invoke(null, new object[] { jsonStream, new byte[6], jsonObject });
            }
            //return
            //    parseString.MakeGenericMethod(entityType)
            //    .Invoke(null, new object[] { json });
        }
        /// <summary>
        /// 反序列化字节数组为指定类型的实体实例
        /// </summary>
        /// <param name="json">字节数组</param>
        /// <param name="entityType">实体类型</param>
        /// <returns>实体实例</returns>
        public static object Parse(byte[] json, Type entityType) {
            return
                parseBytes.MakeGenericMethod(entityType)
                .Invoke(null, new object[] { json });
        }

        static MethodInfo parseString =
                (new Call<JsonHelper, string>(Parse<JsonHelper>)).Method.GetGenericMethodDefinition();
        /// <summary>
        /// 反序列化Json字符串为实体实例
        /// </summary>
        /// <typeparam name="Tentity">实体类型</typeparam>
        /// <param name="json">Json字符串</param>
        /// <returns>实体实例</returns>
        public static Tentity Parse<Tentity>(string json) {
            return JsonHelper.Parse<Tentity>(Encoding.UTF8.GetBytes(json));
        }

        static MethodInfo parseBytes =
            new Call<JsonHelper, byte[]>(Parse<JsonHelper>).Method.GetGenericMethodDefinition();
        /// <summary>
        /// 反序列化字节数组为实体实例
        /// </summary>
        /// <typeparam name="Tentity">实体类型</typeparam>
        /// <param name="json">字节数组</param>
        /// <returns>实体实例</returns>
        public static Tentity Parse<Tentity>(byte[] json) {
            MemoryStream jsonStream = new MemoryStream(json);

            Tentity jsonObject = Class<Tentity>.Create();

            Parse<Tentity>(jsonStream, new byte[6], jsonObject);

            return jsonObject;
        }

        /// <summary>
        /// 反序列化流为实体实例
        /// </summary>
        /// <typeparam name="Tentity">实体类型</typeparam>
        /// <param name="jsonStream">流</param>
        /// <returns>实体实例</returns>
        public static Tentity Parse<Tentity>(Stream jsonStream) {
            Tentity jsonObject = Class<Tentity>.Create();

            Parse<Tentity>(jsonStream, new byte[6], jsonObject);

            return jsonObject;
        }
        /// <summary>
        /// 反序列化流为实体实例
        /// </summary>
        /// <typeparam name="Tentity">实体类型</typeparam>
        /// <param name="jsonStream">流</param>
        /// <param name="jsonObject">实体实例</param>
        public static void Parse<Tentity>(Stream jsonStream, Tentity jsonObject) {
            Parse<Tentity>(jsonStream, new byte[6], jsonObject);
        }
        #endregion Parse
    }
}
