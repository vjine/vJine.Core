﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vJine.Core.IO {
    /// <summary>
    /// 可用标记特性
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple=false)]
    public class ActiveFlagAttribute : Attribute {
        /// <summary>
        /// 实例化可用标记特性
        /// </summary>
        public ActiveFlagAttribute() {
            this.Default = null;
        }
        /// <summary>
        /// 依据指定默认值实例化可用标记特性
        /// </summary>
        /// <param name="Default">默认值</param>
        public ActiveFlagAttribute(bool Default) {
            this.Default = Default;
        }

        public bool? Default { get; set; }
    }
}
