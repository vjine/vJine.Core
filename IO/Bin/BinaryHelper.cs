﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

using vJine.Core.IoC;
using vJine.Core.ORM;

namespace vJine.Core.IO.Bin {
    /// <summary>
    /// 二进制序列化、反序列化帮助类
    /// </summary>
    /// <typeparam name="Tentity">实体类</typeparam>
    public partial class BinHelper<Tentity> {
        /// <summary>
        /// 实例化帮助类
        /// </summary>
        public BinHelper() {
        }

        Stream binaryStream = null;
        /// <summary>
        /// 以指定的流实例化帮助类
        /// </summary>
        /// <param name="binaryStream">流</param>
        public BinHelper(Stream binaryStream) {
            this.binaryStream = binaryStream;
        }
        /// <summary>
        /// 将实体类实例写入到流中
        /// </summary>
        /// <param name="entity">实体实例</param>
        /// <returns>写入的字节数</returns>
        public int Write(Tentity entity) {
            return this.Write(entity, this.binaryStream);
        }
        /// <summary>
        /// 将实体类实例写入到指定的流中
        /// </summary>
        /// <param name="entity">实体类实例</param>
        /// <param name="stream">流</param>
        /// <returns>写入的字节数</returns>
        public int Write(Tentity entity, Stream stream) {
            if(stream == null) {
                throw new ArgumentNullException("stream", "stream is Null");
            }

            BinHelper<Tentity>.ToString(entity, stream);

            return -1;
        }
        /// <summary>
        /// 从流中读取实体实例
        /// </summary>
        /// <returns>实体实例</returns>
        public Tentity Read() {
            if(this.binaryStream == null) {
                throw new CoreException("binaryStream is Null");
            }

            return BinHelper<Tentity>.Parse(this.binaryStream);
        }
        /// <summary>
        /// 从指定的流中读取实体实例
        /// </summary>
        /// <param name="stream">流</param>
        /// <returns>实体实例</returns>
        public Tentity Read(Stream stream) {
            if(stream == null) {
                throw new ArgumentNullException("stream", "stream is Null");
            }

            return BinHelper<Tentity>.Parse(stream);
        }
        /// <summary>
        /// 从指定的流中读取实体实例
        /// </summary>
        /// <param name="stream">流</param>
        /// <param name="entity">实体实例</param>
        public void Read(Stream stream, Tentity entity) {
            BinHelper<Tentity>.Parse(stream, entity);
        }
        /// <summary>
        /// 将缓存中的字节写入到流中
        /// </summary>
        public void Flush() {
            if(this.binaryStream == null) {
                throw new CoreException("binaryStream is Null");
            }

            this.binaryStream.Flush();
        }

        static readonly Type T_nullable = typeof(Nullable<int>).GetGenericTypeDefinition();
        static readonly Type Tobj = typeof(Tentity);
        static readonly Type Tthis = typeof(BinHelper<Tentity>).GetGenericTypeDefinition();

        internal static readonly Exec<Tentity, Stream> toStringHelper = null;
        internal static readonly Call<Tentity, Stream, Tentity> parseHelper = null;

        static BinHelper() {
            try {
                BinHelper<Tentity>.toStringHelper = BinHelper<Tentity>.ToString(Class<Tentity>.GetMap());
            } catch(Exception ex) {
                BinHelper<Tentity>.toStringHelper = (Tentity T_obj, Stream stream) => {
                    throw ex;
                };
            }

            try {
                BinHelper<Tentity>.parseHelper = BinHelper<Tentity>.Parse(Class<Tentity>.GetMap());
            } catch(Exception ex) {
                BinHelper<Tentity>.parseHelper = (Stream stream, Tentity data) => {
                    throw ex;
                };
            }
        }
        /// <summary>
        /// 序列化实体实例为字节数组
        /// </summary>
        /// <param name="entity">实体实例</param>
        /// <returns>字节数组</returns>
        public static byte[] ToString(Tentity entity) {
            MemoryStream mmStream = new MemoryStream();
            BinHelper<Tentity>.toStringHelper(entity, mmStream);
            return mmStream.ToArray();
        }
        /// <summary>
        /// 序列化实体实例到指定的流
        /// </summary>
        /// <param name="entity">实体实例</param>
        /// <param name="stream">流</param>
        public static void ToString(Tentity entity, Stream stream) {
            BinHelper<Tentity>.toStringHelper(entity, stream);
        }

        /// <summary>
        /// 序列化实体实力到指定的文件
        /// </summary>
        /// <param name="entity">实体shilling</param>
        /// <param name="file">文件名</param>
        public static void ToString(Tentity entity, string file) {
            FileStream fStream = File.Open(file, FileMode.Create);
            try {
                BinHelper<Tentity>.ToString(entity, fStream);
            } finally {
                fStream.Close();
            }
        }
        /// <summary>
        /// 反序列化指定的字节数组为实体实例
        /// </summary>
        /// <param name="bytes">字节数组</param>
        /// <returns>实体实例</returns>
        public static Tentity Parse(byte[] bytes) {
            MemoryStream mm = new MemoryStream(bytes);
            return BinHelper<Tentity>.Parse(mm);
        }
        /// <summary>
        /// 反序列化指定的字节数组为实体实例
        /// </summary>
        /// <param name="bytes">字节数组</param>
        /// <param name="entity">实体实例</param>
        public static void Parse(byte[] bytes, Tentity entity) {
            MemoryStream mm = new MemoryStream(bytes);
            BinHelper<Tentity>.Parse(mm, entity);
        }
        /// <summary>
        /// 反序列化指定的流为实体实例
        /// </summary>
        /// <param name="stream">流</param>
        /// <returns>实体实例</returns>
        public static Tentity Parse(Stream stream) {
            return BinHelper<Tentity>.parseHelper(stream, Class.Create<Tentity>());
        }

        /// <summary>
        /// 反序列化指定的文件为实体实力
        /// </summary>
        /// <param name="file">文件</param>
        /// <returns>实体实例</returns>
        public static Tentity Parse(string file) {
            FileStream fStream = File.Open(file, FileMode.Open, FileAccess.Read);
            try {
                return BinHelper<Tentity>.Parse(fStream);
            } finally {
                fStream.Close();
            }
        }
        /// <summary>
        /// 反序列化指定的流为实体实例
        /// </summary>
        /// <param name="stream">流</param>
        /// <param name="entity">实体实例</param>
        public static void Parse(Stream stream, Tentity entity) {
            BinHelper<Tentity>.parseHelper(stream, entity);
        }

        /// <summary>
        /// 反序列化指定的文件为实体实例
        /// </summary>
        /// <param name="file">文件</param>
        /// <param name="entity">实体实力</param>
        public static void Parse(string file, Tentity entity) {
            FileStream fStream = File.Open(file, FileMode.Open, FileAccess.Read);
            try {
                BinHelper<Tentity>.Parse(fStream, entity);
            } finally {
                fStream.Close();
            }
        }
    }
}
