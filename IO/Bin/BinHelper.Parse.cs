﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using vJine.Core.IoC;
using vJine.Core.ORM;

namespace vJine.Core.IO.Bin {
    public partial class BinHelper<Tentity> {

        public static Call<Tentity, Stream, Tentity> Parse(params Class<Tentity>.Property[] P) {
            DynamicMethod dm_parse =
                new DynamicMethod("", BinHelper<Tentity>.Tobj, new Type[] { Reflect.@Stream, BinHelper<Tentity>.Tobj }, true);

            ILGenerator ilGen = dm_parse.GetILGenerator();

            LocalBuilder lvIsNull = ilGen.DeclareLocal(Reflect.@byteArray);
            ilGen.Emit(OpCodes.Ldc_I4_1);
            ilGen.Emit(OpCodes.Newarr, Reflect.@byte);
            ilGen.Emit(OpCodes.Stloc, lvIsNull);

            MethodInfo m_utility_read =
                new Call<int, Stream, byte[], int>(Utility.Read).Method;
            Exec gen_read_isNull = () => {
                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Ldloc, lvIsNull);
                ilGen.Emit(OpCodes.Ldc_I4_1);
                ilGen.Emit(OpCodes.Call, m_utility_read);
                ilGen.Emit(OpCodes.Pop);

                ilGen.Emit(OpCodes.Ldloc, lvIsNull);
                ilGen.Emit(OpCodes.Ldc_I4_0);
                ilGen.Emit(OpCodes.Ldelem_U1);
                ilGen.Emit(OpCodes.Ldc_I4_1);
                ilGen.Emit(OpCodes.Ceq);
            };

            MethodInfo m_get_len =
                    BinHelperCache.GetValue(Reflect.@int);
            Exec gen_read_len = () => {
                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Call, m_get_len);
            };

            #region gen_properties

            Exec<LocalBuilder> _genPropertyParse = (LocalBuilder lvInstance) => {

                for(int i = 0, len = P.Length; i < len; i++) {
                    Class<Tentity>.Property p_i = P[i];
                    if(p_i.IsXmlIgnore) {
                        continue;
                    }

                    Type p_type =
                        p_i.IsNullable ? p_i.pTypeNull : p_i.pType;

                    if(p_i.IsPrimitive) {
                        MethodInfo m_getValue = null;
                        if(p_i.IsEnum) {
                            m_getValue =
                                BinHelperCache.GetValue(Enum.GetUnderlyingType(p_type));
                        } else {
                            m_getValue = BinHelperCache.GetValue(p_type);
                        }
                        if(m_getValue == null) {
                            throw new CoreException("[{0}]:m_getValue Is Nullable", Reflect.GetTypeName(p_i.pType));
                        }

                        ilGen.Emit(OpCodes.Ldloc, lvInstance);
                        {
                            if(p_i.IsNullable) {
                                Label lbIsNull = ilGen.DefineLabel();
                                Label lbIsNotNull = ilGen.DefineLabel();
                                gen_read_isNull();
                                ilGen.Emit(OpCodes.Brtrue, lbIsNull);
                                ilGen.Emit(OpCodes.Ldarg_0);
                                ilGen.Emit(OpCodes.Call, m_getValue);
                                ilGen.Emit(OpCodes.Newobj, p_i.ctorNull);
                                ilGen.Emit(OpCodes.Br, lbIsNotNull);

                                ilGen.MarkLabel(lbIsNull);
                                {
                                    LocalBuilder lvNull = ilGen.DeclareLocal(p_i.pType);
                                    ilGen.Emit(OpCodes.Ldloca, lvNull);
                                    ilGen.Emit(OpCodes.Initobj, p_i.pType);
                                    ilGen.Emit(OpCodes.Ldloc, lvNull);
                                }

                                ilGen.MarkLabel(lbIsNotNull);
                            } else {
                                ilGen.Emit(OpCodes.Ldarg_0);
                                ilGen.Emit(OpCodes.Call, m_getValue);
                            }
                        }
                        ilGen.Emit(OpCodes.Call, p_i.set);
                    } else {
                        MethodInfo binary_parse =
                            Tthis.MakeGenericType(p_type).GetMethod("Parse", new Type[] { Reflect.@Stream });

                        ilGen.Emit(OpCodes.Ldloc, lvInstance);
                        {
                            ilGen.Emit(OpCodes.Ldarg_0);
                            ilGen.Emit(OpCodes.Call, binary_parse);
                        }
                        ilGen.Emit(OpCodes.Call, p_i.set);
                    }
                }
            };
            #endregion gen_properties

            if(Reflect.IsBaseType(BinHelper<Tentity>.Tobj)) {
                MethodInfo m_getValue = null;
                if(Reflect.IsEnum(BinHelper<Tentity>.Tobj)) {
                    m_getValue =
                        BinHelperCache.GetValue(Enum.GetUnderlyingType(BinHelper<Tentity>.Tobj));
                } else {
                    m_getValue = BinHelperCache.GetValue(BinHelper<Tentity>.Tobj);
                }
                if(m_getValue == null) {
                    throw new CoreException("[{0}]:m_getValue Is Nullable", Class<Tentity>.FullName);
                }

                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Call, m_getValue);
            } else {
                Label lbIsNotNull = ilGen.DefineLabel();
                gen_read_isNull();
                {//if(objT == null) return null;
                    ilGen.Emit(OpCodes.Brfalse, lbIsNotNull);
                    ilGen.Emit(OpCodes.Ldnull);
                    ilGen.Emit(OpCodes.Ret);
                } // else return value {
                ilGen.MarkLabel(lbIsNotNull);
                {
                    if(BinHelper<Tentity>.Tobj == Reflect.@object) {
                        Call<object, Stream, object> obj_parse = (Stream stream, object data) => {
                            string type_name = BinHelper<string>.Parse(stream);
                            Type T_obj = Reflect.GetType(type_name);
                            if(T_obj == Reflect.@object) {
                                throw new CoreException("Type Shouldn't Be Object");
                            }

                            //TODO:检查IBinarizer接口
                            Type T_objToString = BinHelper<Tentity>.Tthis.MakeGenericType(T_obj);
                            Delegate D =
                                T_objToString.GetField("parseHelper", BindingFlags.Public | BindingFlags.Static).GetValue(null) as Delegate;
                            return D.DynamicInvoke(stream, data);
                        };

                        ilGen.Emit(OpCodes.Ldarg_0);
                        ilGen.Emit(OpCodes.Ldarg_1);
                        ilGen.Emit(OpCodes.Call, obj_parse.Method);
                    } else if(Reflect.IsArray(BinHelper<Tentity>.Tobj)) {
                        int array_rank = BinHelper<Tentity>.Tobj.GetArrayRank();
                        if(array_rank != 1) {
                            throw new CoreException("Just 1 Dimension Array Supported!");
                        }

                        Type T_item = BinHelper<Tentity>.Tobj.GetElementType();
                        LocalBuilder lvArray =
                            ilGen.DeclareLocal(BinHelper<Tentity>.Tobj);
                        LocalBuilder lv_array_len = ilGen.DeclareLocal(Reflect.@int);
                        {
                            gen_read_len();
                            ilGen.Emit(OpCodes.Stloc, lv_array_len);

                            ilGen.Emit(OpCodes.Ldloc, lv_array_len);
                            ilGen.Emit(OpCodes.Newarr, T_item);
                            ilGen.Emit(OpCodes.Stloc, lvArray);
                        }

                        if(BinHelper<Tentity>.Tobj == Reflect.@byteArray) {
                            ilGen.Emit(OpCodes.Ldarg_0);
                            ilGen.Emit(OpCodes.Ldloc, lvArray);
                            ilGen.Emit(OpCodes.Ldloc, lv_array_len);
                            ilGen.Emit(OpCodes.Call, m_utility_read);
                            ilGen.Emit(OpCodes.Pop);
                        } else {
                            MethodInfo array_parse =
                                BinHelperCache.array_parse.MakeGenericMethod(BinHelper<Tentity>.Tobj.GetElementType());
                            ilGen.Emit(OpCodes.Ldloc, lvArray);
                            ilGen.Emit(OpCodes.Ldloc, lv_array_len);
                            ilGen.Emit(OpCodes.Ldarg_0);
                            Emit.Opc_Call(ilGen, array_parse);
                        }

                        ilGen.Emit(OpCodes.Ldloc, lvArray);
                    } else {
                        LocalBuilder lvInstance =
                            ilGen.DeclareLocal(BinHelper<Tentity>.Tobj);
                        Label lbInstanceIsNotNull = ilGen.DefineLabel();
                        Label lbParseInstance = ilGen.DefineLabel();

                        ilGen.Emit(OpCodes.Ldarg_1);
                        ilGen.Emit(OpCodes.Ldnull);
                        ilGen.Emit(OpCodes.Ceq);
                        ilGen.Emit(OpCodes.Brfalse, lbInstanceIsNotNull);
                        //if (instance is null) {
                        {//Init Instance
                            Emit.Opc_Init(ilGen, BinHelper<Tentity>.Tobj);
                            ilGen.Emit(OpCodes.Stloc, lvInstance);
                            ilGen.Emit(OpCodes.Br, lbParseInstance);
                        } //} else {
                        {//Load Instance
                            ilGen.MarkLabel(lbInstanceIsNotNull);
                            ilGen.Emit(OpCodes.Ldarg_1);
                            ilGen.Emit(OpCodes.Stloc, lvInstance);
                        }
                        ilGen.MarkLabel(lbParseInstance);
                        {
                            _genPropertyParse(lvInstance);

                            if(Reflect.IsList(BinHelper<Tentity>.Tobj)) {
                                MethodInfo list_parse =
                                    BinHelperCache.list_parse.MakeGenericMethod(Reflect.GetGenericArgs(BinHelper<Tentity>.Tobj));
                                ilGen.Emit(OpCodes.Ldloc, lvInstance);
                                gen_read_len();
                                ilGen.Emit(OpCodes.Ldarg_0);
                                Emit.Opc_Call(ilGen, list_parse);
                            } else if(Reflect.IsDictionary(BinHelper<Tentity>.Tobj)) {
                                MethodInfo dict_parse =
                                    BinHelperCache.dict_parse.MakeGenericMethod(Reflect.GetGenericArgs(BinHelper<Tentity>.Tobj));
                                ilGen.Emit(OpCodes.Ldloc, lvInstance);
                                gen_read_len();
                                ilGen.Emit(OpCodes.Ldarg_0);
                                Emit.Opc_Call(ilGen, dict_parse);
                            }
                        }

                        ilGen.Emit(OpCodes.Ldloc, lvInstance);
                    }
                }
            }

            ilGen.Emit(OpCodes.Ret);
            return dm_parse.CreateDelegate(typeof(Call<Tentity, Stream, Tentity>)) as Call<Tentity, Stream, Tentity>;
        }
    }
}
