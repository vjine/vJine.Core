﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using vJine.Core.IoC;
using vJine.Core.ORM;

namespace vJine.Core.IO.Bin {
    public partial class BinHelper<Tentity> {
        public static Exec<Tentity, Stream> ToString(params Class<Tentity>.Property[] P) {
            DynamicMethod dmToString =
                new DynamicMethod("", Reflect.@void, new Type[] { BinHelper<Tentity>.Tobj, Reflect.@Stream }, true);
            MethodInfo stream_write =
                typeof(Stream).GetMethod("Write", new Type[] { Reflect.@byteArray, Reflect.@int, Reflect.@int });
            Dictionary<Type, LocalBuilder> lvs = new Dictionary<Type, LocalBuilder>();

            ILGenerator ilGen = dmToString.GetILGenerator();

            Exec<bool> gen_write_isNull = (bool IsNull) => {
                ilGen.Emit(IsNull ? OpCodes.Ldc_I4_1 : OpCodes.Ldc_I4_0);
                ilGen.Emit(OpCodes.Ldarg_1);
                ilGen.Emit(OpCodes.Call, BinHelperCache.GetBytes(Reflect.@bool));
            };

            #region gen_properties

            Exec _genPropertyToString = () => {
                for(int i = 0, len = P.Length; i < len; i++) {
                    Class<Tentity>.Property p_i = P[i];
                    Type p_type =
                        p_i.IsNullable ? p_i.pTypeNull : p_i.pType;

                    if(p_i.IsXmlIgnore) {
                        continue;
                    }

                    Label lbNextProperty = ilGen.DefineLabel();
                    if(p_i.IsPrimitive) {
                        MethodInfo m_getBytes = null;
                        if(p_i.IsEnum) {
                            Type enum_type = Enum.GetUnderlyingType(p_type);
                            m_getBytes =
                                BinHelperCache.GetBytes(enum_type);
                        } else {
                            m_getBytes = BinHelperCache.GetBytes(p_type);
                        }
                        if(m_getBytes == null) {
                            throw new CoreException("[{0}]:m_getBytes Is Nullable", Reflect.GetTypeName(p_i.pType));
                        }
                        //get_Value
                        if(p_i.IsNullable) {
                            Label lbHasValue = ilGen.DefineLabel();
                            LocalBuilder lvNull = ilGen.DeclareLocal(p_i.pType);
                            Type Tnull = T_nullable.MakeGenericType(p_i.pTypeNull);

                            ilGen.Emit(OpCodes.Ldarg_0);
                            Emit.Opc_Call(ilGen, p_i.get);
                            ilGen.Emit(OpCodes.Stloc, lvNull);
                            ilGen.Emit(OpCodes.Ldloca, lvNull);
                            ilGen.Emit(OpCodes.Call, Tnull.GetProperty("HasValue").GetGetMethod());
                            ilGen.Emit(OpCodes.Brtrue, lbHasValue);
                            gen_write_isNull(true);
                            ilGen.Emit(OpCodes.Br, lbNextProperty);

                            ilGen.MarkLabel(lbHasValue);
                            gen_write_isNull(false);
                            ilGen.Emit(OpCodes.Ldloca, lvNull);
                            ilGen.Emit(OpCodes.Call, Tnull.GetProperty("Value").GetGetMethod());
                        } else {
                            ilGen.Emit(OpCodes.Ldarg_0);
                            Emit.Opc_Call(ilGen, p_i.get);
                        }

                        ilGen.Emit(OpCodes.Ldarg_1);
                        ilGen.Emit(OpCodes.Call, m_getBytes);
                    } else {
                        ilGen.Emit(OpCodes.Ldarg_0);
                        Emit.Opc_Call(ilGen, p_i.get);

                        MethodInfo binary_tostring =
                            BinHelper<Tentity>.Tthis.MakeGenericType(p_type).GetMethod("ToString", new Type[] { p_type, Reflect.@Stream });

                        ilGen.Emit(OpCodes.Ldarg_1);
                        Emit.Opc_Call(ilGen, binary_tostring);
                    }
                    ilGen.MarkLabel(lbNextProperty);
                }
            };
            #endregion gen_properties

            if(Reflect.IsBaseType(BinHelper<Tentity>.Tobj)) {
                MethodInfo m_getBytes = null;
                if(Reflect.IsEnum(BinHelper<Tentity>.Tobj)) {
                    Type enum_type = Enum.GetUnderlyingType(BinHelper<Tentity>.Tobj);
                    Emit.Opc_Conv(ilGen, enum_type);
                    m_getBytes =
                        BinHelperCache.GetBytes(enum_type);
                } else {
                    m_getBytes = BinHelperCache.GetBytes(BinHelper<Tentity>.Tobj);
                }
                if(m_getBytes == null) {
                    throw new CoreException("[{0}]:m_getBytes Is Nullable", Class<Tentity>.FullName);
                }

                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Ldarg_1);
                ilGen.Emit(OpCodes.Call, m_getBytes);
            } else {
                Label lbIsNotNull = ilGen.DefineLabel();
                {//if(objT == null) return write 1;
                    ilGen.Emit(OpCodes.Ldarg_0);
                    ilGen.Emit(OpCodes.Ldnull);
                    ilGen.Emit(OpCodes.Ceq);

                    ilGen.Emit(OpCodes.Brfalse, lbIsNotNull);
                    {
                        gen_write_isNull(true);
                        ilGen.Emit(OpCodes.Ret);
                    }
                } // else write 0 & value {
                ilGen.MarkLabel(lbIsNotNull);
                gen_write_isNull(false);
                {
                    if(BinHelper<Tentity>.Tobj == Reflect.@object) {
                        Exec<object, Stream> obj_toString = (object obj, Stream stream) => {
                            Type T_obj = obj.GetType();

                            BinHelper<string>.ToString(Reflect.GetTypeName(T_obj), stream);

                            Type T_objToString = BinHelper<Tentity>.Tthis.MakeGenericType(T_obj);
                            Delegate D =
                                T_objToString.GetField("toStringHelper", BindingFlags.Public | BindingFlags.Static).GetValue(null) as Delegate;
                            D.DynamicInvoke(obj, stream);
                        };

                        ilGen.Emit(OpCodes.Ldarg_0);
                        ilGen.Emit(OpCodes.Ldarg_1);
                        ilGen.Emit(OpCodes.Call, obj_toString.Method);
                    } else if(Reflect.IsArray(BinHelper<Tentity>.Tobj)) {
                        ilGen.Emit(OpCodes.Ldarg_0);
                        ilGen.Emit(OpCodes.Ldlen);
                        ilGen.Emit(OpCodes.Ldarg_1);
                        ilGen.Emit(OpCodes.Call, BinHelperCache.GetBytes(Reflect.@int));

                        if(BinHelper<Tentity>.Tobj == Reflect.@byteArray) {
                            ilGen.Emit(OpCodes.Ldarg_1);
                            ilGen.Emit(OpCodes.Ldarg_0);
                            ilGen.Emit(OpCodes.Ldc_I4_0);
                            ilGen.Emit(OpCodes.Ldarg_0);
                            ilGen.Emit(OpCodes.Ldlen);
                            ilGen.Emit(OpCodes.Callvirt,
                                typeof(Stream).GetMethod("Write", new Type[] { typeof(byte[]), typeof(int), typeof(int) }));
                        } else {
                            MethodInfo array_tostring =
                                BinHelperCache.array_toString.MakeGenericMethod(BinHelper<Tentity>.Tobj.GetElementType());
                            ilGen.Emit(OpCodes.Ldarg_0);
                            ilGen.Emit(OpCodes.Ldarg_1);
                            Emit.Opc_Call(ilGen, array_tostring);
                        }
                    } else {
                        _genPropertyToString();

                        if(Reflect.IsList(BinHelper<Tentity>.Tobj)) {
                            ilGen.Emit(OpCodes.Ldarg_0);
                            Emit.Opc_Call(ilGen,
                                BinHelper<Tentity>.Tobj.GetProperty("Count").GetGetMethod(true));
                            ilGen.Emit(OpCodes.Ldarg_1);
                            ilGen.Emit(OpCodes.Call,
                                BinHelperCache.GetBytes(Reflect.@int));

                            MethodInfo list_tostring =
                                BinHelperCache.list_toString.MakeGenericMethod(Reflect.GetGenericArgs(BinHelper<Tentity>.Tobj));
                            ilGen.Emit(OpCodes.Ldarg_0);
                            ilGen.Emit(OpCodes.Ldarg_1);
                            Emit.Opc_Call(ilGen, list_tostring);
                        } else if(Reflect.IsDictionary(BinHelper<Tentity>.Tobj)) {
                            ilGen.Emit(OpCodes.Ldarg_0);
                            Emit.Opc_Call(ilGen,
                                BinHelper<Tentity>.Tobj.GetProperty("Count").GetGetMethod(true));
                            ilGen.Emit(OpCodes.Ldarg_1);
                            ilGen.Emit(OpCodes.Call,
                                BinHelperCache.GetBytes(Reflect.@int));

                            MethodInfo dict_tostring =
                                       BinHelperCache.dict_toString.MakeGenericMethod(Reflect.GetGenericArgs(BinHelper<Tentity>.Tobj));
                            ilGen.Emit(OpCodes.Ldarg_0);
                            ilGen.Emit(OpCodes.Ldarg_1);
                            Emit.Opc_Call(ilGen, dict_tostring);
                        }
                    }
                }
            }

            ilGen.Emit(OpCodes.Ret);
            return dmToString.CreateDelegate(typeof(Exec<Tentity, Stream>)) as Exec<Tentity, Stream>;
        }
    }
}
