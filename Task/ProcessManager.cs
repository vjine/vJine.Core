﻿using System;
using System.Collections.Generic;
using System.Text;
using vJine.Core.Base;

namespace vJine.Core.Task {
    /// <summary>
    /// 进程管理器
    /// </summary>
    public class ProcessManager {
        /// <summary>
        /// 进程配置结合
        /// </summary>
        public CollectionBase<ProcessConfig> Configs { get; set; }

        TaskQueue<ProcessConfig> daemon = null;
        /// <summary>
        /// 实例化进程管理器
        /// </summary>
        public ProcessManager() {
            this.daemon = new TaskQueue<ProcessConfig>(1, 50, null, (ProcessConfig pConfig) => {
                if(pConfig.IsRunning) {
                    return;
                }

                pConfig.Start();
            });
            this.daemon.Start();
        }

        /// <summary>
        /// 按名字迭代进程配置
        /// </summary>
        /// <param name="name">进程名称</param>
        /// <returns>进程配置</returns>
        public ProcessConfig this[string name] {
            get {
                if(string.IsNullOrEmpty(name)) {
                    return null;
                }
                if(this.Configs == null || this.Configs.Count == 0) {
                    return null;
                }

                for(int i = 0, len = this.Configs.Count; i < len; i++) {
                    ProcessConfig pConfig = this.Configs[i];
                    if(pConfig.name == name) {
                        return pConfig;
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// 初始化设置
        /// </summary>
        public void Init() {
            if(this.Configs == null || this.Configs.Count == 0) {
                return;
            }

            for(int i = 0, len = this.Configs.Count; i < len; i++) {
                ProcessConfig pConfig = this.Configs[i];
                pConfig.OnExit += (ProcessConfig config) => {
                    this.daemon.Enqueue(config);
                };
            }
        }

        /// <summary>
        /// 按指定进程名启动进程，如果不指定名称则启动所有未启动的进程
        /// </summary>
        /// <param name="name">进程名</param>
        public void Start(string name = null) {
            this.action(name, (ProcessConfig pConfig) => {
                pConfig.Start();
            });
        }

        /// <summary>
        /// 按指定进程名停止进程，如果不指定名称则启动所有未停止的进程
        /// </summary>
        /// <param name="name">进程名</param>
        public void Stop(string name = null) {
            this.action(name, (ProcessConfig pConfig) => {
                pConfig.Stop();
            });
        }

        /// <summary>
        /// 按指定进程名重启进程，如果不指定名称则重启所有运行的进程
        /// </summary>
        /// <param name="name">进程名</param>
        public void Restart(string name = null) {
            this.action(name, (ProcessConfig pConfig) => {
                pConfig.Restart();
            });
        }

        void action(string name, Exec<ProcessConfig> do_action) {
            lock(this) {
                if(string.IsNullOrEmpty(name)) {
                    for(int i = 0, len = this.Configs.Count; i < len; i++) {
                        ProcessConfig pConfig = this.Configs[i];

                        try {
                            do_action(pConfig);
                        } finally {
                        }
                    }
                } else {
                    ProcessConfig pConfig = this[name];
                    if(pConfig == null) {
                        return;
                    }

                    do_action(pConfig);
                }
            }
        }
    }
}
