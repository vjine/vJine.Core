﻿using System;
using System.Collections.Generic;
using System.Text;
using vJine.Core.ORM;

namespace vJine.Core.Plugin {
    public class IPluggable {
        public string name { get; set; }
        public string version { get; set; }
        public string path { get; set; }

        public virtual void load(params object[] args) {
            throw new NotImplementedException(Class<IPluggable>.Name + ".load");
        }

        public virtual void reg_methods(PluginManager pm) {
        }
    }
}
