﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using vJine.Core.IoC;

namespace vJine.Core.Plugin {
    /// <summary>
    /// 插件管理类
    /// </summary>
    public class PluginManager {
        Dictionary<string, Delegate> methods = new Dictionary<string, Delegate>();
        public bool reg_method(string name, Delegate method) {
            if (this.methods.ContainsKey(name)) {
                return false;
            }

            this.methods.Add(name, method); return true;
        }

        /// <summary>
        /// 加载插件
        /// </summary>
        /// <param name="path">插件路径或产检文件名</param>
        /// <returns>已加载插件的数量</returns>
        public int load(string path) {
            return this.load(path, null, null);
        }

        /// <summary>
        /// 加载插件
        /// </summary>
        /// <param name="path">插件路径或产检文件名</param>
        /// <param name="pattern">插件名称匹配模式</param>
        /// <param name="args">加载参数</param>
        /// <returns>已加载插件的数量</returns>
        public int load(string path, string pattern, params object[] args) {
            List<string> plugin_files = new List<string>();
            if (File.Exists(path)) {
                plugin_files.Add(path);
            } else if(Directory.Exists(path)) {
                string[] dll_files =
                    Directory.GetFiles(path, "*.dll", SearchOption.TopDirectoryOnly);
                if (dll_files != null && dll_files.Length > 0) {
                    for (int i = 0, len = dll_files.Length; i < len; i++) {
                        plugin_files.Add(dll_files[i]);
                    }
                }
            }
            if (plugin_files.Count == 0) {
                return 0;
            }

            int load_counter = 0;
            for (int i = 0, len = plugin_files.Count; i < len; i++) {
                Assembly asmPlugin = this.get_assembly(plugin_files[i]);

                Type[] typePlugin = asmPlugin.GetTypes();
                for (int j = 0, len_j = typePlugin.Length; j < len_j; j++) {
                    Type type_j = typePlugin[j];
                    if (type_j.BaseType == typeof(IPluggable)) {
                        IPluggable plug_j = (IPluggable)Class.Create(type_j);
                        {
                            plug_j.load(args);
                            plug_j.reg_methods(this);
                            load_counter += 1;
                        }
                    }
                }
            }

            return load_counter;
        }

        /// <summary>
        /// 运行插件中注册的方法
        /// </summary>
        /// <param name="name">方法名称</param>
        /// <param name="args">方法参数</param>
        /// <returns>方法返回值</returns>
        public object exec(string name, params object[] args) {
            if (string.IsNullOrEmpty(name)) {
                throw new ArgumentNullException("name");
            }
            if (!this.methods.ContainsKey(name)) {
                throw new NotImplementedException(name);
            }

            return this.methods[name].DynamicInvoke(args);
        }

        /// <summary>
        /// 检查是否存在插件方法
        /// </summary>
        /// <param name="name">方法名称</param>
        /// <returns>存在则返回true</returns>
        public bool has_method(string name) {
            if (string.IsNullOrEmpty(name)) {
                return false;
            }

            return this.methods.ContainsKey(name);
        }

        Assembly get_assembly(string path) {
            Module[] mLoaded = Assembly.GetEntryAssembly().GetLoadedModules();
            if (mLoaded != null && mLoaded.Length > 0) {
                for (int i = 0, len = mLoaded.Length; i < len; i++) {
                    Module module_i = mLoaded[i];
                    string file_path = module_i.Assembly.Location;
                    if (string.IsNullOrEmpty(file_path)) {
                        continue;
                    }
                    if (file_path.ToLower().EndsWith(path.ToLower())) {
                        return module_i.Assembly;
                    }
                }
            }

            return AppDomain.CurrentDomain.Load(File.ReadAllBytes(path));
        }
    }
}
