﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vJine.Core.IoC.Config {
    [AttributeUsage(AttributeTargets.Delegate)]
    public class InvokerAttribute : Attribute {
    }
}
