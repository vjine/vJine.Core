﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace vJine.Core.IoC.Config {
    [AttributeUsage(AttributeTargets.Parameter | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class ParamAttribute : Attribute {
        public ParamAttribute()
            : this(null, null, null) {
            //Get Parameter Name As Default Value if this.Name is null or empty
        }

        public ParamAttribute(string Name)
            : this(Name, null, null) {

        }

        public ParamAttribute(string Name, string Comments)  {
            this.Name = Name;
            this.Comment = Comments;
        }

        public ParamAttribute(string Name, string[] Patterns, string[] Values) {
            this.Name = Name;
            this.IsRequired = true;

            this.Patterns = Patterns;
            this.Values = Values;
        }

        public string Name { get; set; }

        public Type Class { get; set; }

        public object value { get; set; }

        public string Comment { get; set; }

        public string[] Patterns { get; set; }

        public string[] Values { get; set; }

        public bool CheckPattern() {
            if (Patterns == null && Values == null) {
                return false;
            }

            if (Patterns != null && Values == null ||
                Patterns == null && Values != null ||
                Patterns.Length != Values.Length) {
                return false;
            }

            return true;
        }

        public bool IsRequired { get; set; }

        public object Get(object objContext) {
            Type tParameter = this.Class;
            string name = this.Name;

            object objParam = Property.Get(objContext, name);
            if(objParam == null && this.IsRequired) {
                throw new CoreException("参数错误:【{0}】,未传递参数", name);
            }

            try {
                if(objParam is string && tParameter != Reflect.@string) {
                    objParam = vJine.Core.IoC.Class.Parse(objParam, tParameter);
                }
            } catch(Exception ex) {
                throw new CoreException("参数错误:【{0}】,无法解析类型,{1}", name, ex.ToString());
            }

            //参数匹配及替换参数
            try {
                if(this.CheckPattern()) {
                    string V = objParam.ToString();
                    for(int j = 0; j < this.Patterns.Length; j++) {
                        if((objParam == null && string.IsNullOrEmpty(this.Patterns[j])) || Regex.IsMatch(V, this.Patterns[j])) {
                            objParam =
                                vJine.Core.IoC.Class.Parse(Property.Get(objContext, this.Values[j]), tParameter);
                            break;
                        }
                    }
                }
            } catch(Exception ex) {
                throw new CoreException("参数错误:【{0}】,{1}", name, ex.ToString());
            }

            return objParam ?? this.value;
        }
    }
}
