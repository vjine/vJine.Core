﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace vJine.Core {
    public class BizException : CoreException {
        /// <summary>
        /// 实例化业务异常
        /// </summary>
        public BizException()
            : base() {

        }
        /// <summary>
        /// 实例化业务异常
        /// </summary>
        /// <param name="ex">导致当前异常的异常</param>
        public BizException(Exception ex)
            : base("", ex) {

        }
        /// <summary>
        /// 实例化业务异常
        /// </summary>
        /// <param name="msg">异常消息</param>
        /// <param name="args">异常消息参数</param>
        public BizException(string msg, params object[] args)
            : base(string.Format(msg, args)) {
        }
        /// <summary>
        /// 实例化业务异常
        /// </summary>
        /// <param name="ex">导致当前异常的异常</param>
        /// <param name="msg">异常消息</param>
        /// <param name="args">异常消息参数</param>
        public BizException(Exception ex, string msg, params object[] args)
            : base(string.Format(msg, args), ex) {
        }
        /// <summary>
        /// 实例化业务异常
        /// </summary>
        /// <param name="method">产生此异常时的方法</param>
        /// <param name="msg">异常消息</param>
        /// <param name="args">异常消息参数</param>
        public BizException(MethodBase method, string msg, params object[] args)
            : base(string.Format(msg, args)) {
        }
    }
}
