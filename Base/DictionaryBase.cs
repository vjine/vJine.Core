﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vJine.Core.Base {
    /// <summary>
    /// 实体类型键值集合基类
    /// </summary>
    /// <typeparam name="TKey">键实例类</typeparam>
    /// <typeparam name="TValue">属性实例类</typeparam>
    public class DictionaryBase<TKey, TValue> : Dictionary<TKey, TValue> {
        /// <summary>
        /// 实例化实体类型键值集合
        /// </summary>
        public DictionaryBase() {
        }
    }
}
