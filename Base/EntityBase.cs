﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Reflection;

namespace vJine.Core.Base {
    /// <summary>
    /// 实体类型基类
    /// </summary>
    [Serializable]
    public class EntityBase : INotifyPropertyChanged {
        /// <summary>
        /// 实例化实体类型基类
        /// </summary>
        public EntityBase() {
        }

        #region INotifyPropertyChanged 成员
        /// <summary>
        /// 属性改变事件
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// 属性改变通知
        /// </summary>
        /// <param name="name">属性名</param>
        protected void NotifyPropertyChanged(string name) {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion
    }
}
