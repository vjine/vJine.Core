﻿using System;
using System.Xml.Serialization;

namespace vJine.Core.MQ {
    public delegate void ClientArrivedEventHandler<T>(ClientArrivedEventArgs<T> Arg);
    public delegate void NewMessageEventHandler<T>(NewMessageEventArgs<T> Arg);

    public class ClientArrivedEventArgs<T> : EventArgs {
        public bool Cancel { get; set; }
        public string Host { get; internal set; }
        public MessageClient<T> Queue { get; internal set; }
    }

    public class NewMessageEventArgs<T> : EventArgs {
        public MessageClient<T> Owner { get; internal set; }
        public MessagePackage<T> Message { get; internal set; }
        public bool Handled { get; set; }
    }

    /// <summary>
    ///队列优先级0-6, 0用于系统内部信号交互, 6用于发送关闭信号，1-5 由外部使用，且默认优先级为3
    //发送关闭信号之前必须发送0优先级的不接收信号，发送成功后方可发送7优先级的关闭信号。
    //如果系统发送明明消息，回复是必须回复同名消息，
    //系统注册消息接收时可指定消息名称，且在绑定事件时指定，并支持正则匹配
    /// </summary>
    public enum MessagePriority : byte {
        P1 = 1,
        P2 = 2,
        P3 = 3,
        P4 = 4,
        P5 = 5
    }

    public class MQException : Exception {
        public MQException(Exception ex)
            : base(ex.Message) {
        }

        public MQException(string Msg, params object[] Args)
            : base(string.Format(Msg, Args)) {

        }

        public MQException(Exception inner, string Msg, params object[] Args)
            : base(string.Format(Msg, Args), inner) {
        }
    }
}
