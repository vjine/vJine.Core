﻿using System;
using System.Threading;
using System.Xml.Serialization;
using vJine.Core.ORM;
using vJine.Core.IoC;

namespace vJine.Core.MQ {
    public enum MessageCommand : byte {
        Close = 0,
        Open = 1,
        Trans = 2,
        Echo = 3,
        Time = 4
    }

    [Serializable]
    public class MessagePackage<T> {
        Call<long, MessagePackage<T>> _write;
        public Call<long, MessagePackage<T>> Write {
            get {
                return this._write;
            }
            set {
                if (value == this._write) {
                    return;
                }

                if (value == null) {
                    this._hashCode = -1;
                } else {
                    this._write = value;
                    this._hashCode = value.GetHashCode();
                }
            }
        }

        [XmlAttribute("Topic")]
        public string Topic { get; set; }

        [XmlAttribute("Cmd")]
        public MessageCommand Cmd { get; set; }

        [XmlElement("Data")]
        public T Data { get; set; }

        internal ManualResetEvent MSent;
        internal Exception exSent;

        int _hashCode = -1;
        public override int GetHashCode() {
            return this._hashCode;
        }
    }
}
