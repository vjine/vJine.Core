﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vJine.Core.Web
{
    public enum HttpMethod : byte
    {
        GET,
        POST,
        HEAD,
        TRACE,
        PUT,
        DELETE,
        OPTIONS,
        CONNECT
    }
}