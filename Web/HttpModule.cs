﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;
using vJine.Core.Base;
using vJine.Core.ORM;

namespace vJine.Core.Web {
    [Serializable]
    public partial class HttpModule {
        [XmlAttribute, Map(IsPrimary = true)]
        public string Name { get; set; }
        [XmlAttribute]
        public string Version { get; set; }
        [XmlAttribute]
        public string Author { get; set; }

        [XmlAttribute]
        public bool IsSys { get; set; }
        public partial class _ {
            public static readonly Property<HttpModule, bool> IsSys = new Property<HttpModule, bool>("IsSys");
        }

        [XmlAttribute, Map(IsNullable = true)]
        public string Comment { get; set; }

        [XmlAttribute]
        public bool IsActive { get; set; }
        public partial class _ {
            public static readonly Property<HttpModule, bool> IsActive = new Property<HttpModule, bool>("IsActive");
        }

        [XmlAttribute]
        public DateTime UploadDate { get; set; }
        [XmlAttribute]
        public DateTime? InstallDate { get; set; }
        [XmlAttribute]
        public DateTime? UpdateDate { get; set; }

        [XmlIgnore, Map(IsNullable = true)]
        public byte[] Bin { get; set; }
        [XmlArray]
        public CollectionBase<HttpAction> Actions { get; set; }

        [XmlIgnore,Map(IsIgnored=true)]
        public Assembly Assembly { get; set; }
    }
}
