﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using vJine.Core;
using vJine.Core.IoC;

namespace vJine.Core.Web {
    [Serializable]
    public class HttpData<T> {
        /// <summary>
        /// Status
        /// </summary>
        [XmlAttribute("S")]
        public string S { get; set; }
        /// <summary>
        /// Message
        /// </summary>
        [XmlAttribute("M")]
        public string M { get; set; }
        /// <summary>
        /// Data
        /// </summary>
        [XmlElement("D")]
        public T D { get; set; }
    }

    public enum HttpDataStatus : byte {
        /// <summary>
        /// Not Available
        /// </summary>
        NA = 0,
        /// <summary>
        /// OK
        /// </summary>
        OK = 1,
        /// <summary>
        /// Update
        /// </summary>
        UP = 2,
        /// <summary>
        /// Denied
        /// </summary>
        DN = 3,
        /// <summary>
        /// Error
        /// </summary>
        ER = 4
    }

    public class HttpBizException : CoreException {
        public HttpBizException(string Msg, params object[] Args)
            : base(Msg, Args) {

        }
    }
}
