﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vJine.Core.Web.Config {
    [AttributeUsage(AttributeTargets.Parameter, AllowMultiple=false)]
    public class AntiXssAttribute : Attribute {
    }
}
