﻿using System;
using System.IO;

namespace vJine.Core.Web
{
    public class HttpResponse : HttpBase
    {
        public string attach_file_name { get; set; }

        public override void Reset() {
            base.Reset();

            this.attach_file_name = null;
        }
    }
}
