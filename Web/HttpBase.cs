﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using vJine.Core;
using vJine.Core.Base;
using vJine.Core.IoC;

namespace vJine.Core.Web
{
    public partial class HttpBase
    {
        static HttpBase()
        {
            HttpBase.InitHeaders();
        }

        public HttpBase()
        {
            this.Unkonwns = new List<string>();
        }

        public HttpStatus? Status { get; set; }

        public static readonly Encoding ASCII = Encoding.ASCII;
        public static readonly Encoding UTF8 = Encoding.UTF8;

        public static readonly string Version = "HTTP/1.1";
        public static readonly byte[] BNewLine = new byte[] { (byte)'\r', (byte)'\n' };
        public static readonly byte[] BSeperator = new byte[] { (byte)':', (byte)' ' };
        public static readonly byte[] BSpace = new byte[] { (byte)' ' };

        public static readonly byte[] B_Qmark = new byte[] { (byte)'?' };
        public static readonly byte[] B_and = new byte[] { (byte)'&' };
        public static readonly byte[] B_equator = new byte[] { (byte)'=' };

        public static readonly string boundary = "boundary=";

        public virtual void Reset() {
            foreach(KeyValuePair<string, HttpHeader> kv in HttpBase.ClientHeaders) {
                kv.Value.Set(this, null);
            }
        }
    }
}
