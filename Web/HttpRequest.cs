﻿using System.Collections.Generic;
using System.IO;
using vJine.Core.ORM;

namespace vJine.Core.Web
{
    public class HttpRequest : HttpBase
    {
        public HttpRequest(string ip, int port)
        {
            this.C = new Dictionary<string, string>();
            this.Q = new Dictionary<string, string>();
            this.P = new Dictionary<string, string>();
            this.F = new Dictionary<string, FileUpload>();

            this.ip = ip; this.port = port;
            this._dm = new DataManager();
            this._dm.Open();
        }

        public HttpMethod Method { get; set; }

        public string user { get; set; }

        public string ip { get; set; }
        public int port { get; set; }

        public string Path { get; set; } //* /123.htm
        public string FullPath { get; set; } //* V:/www/123.htm
        public string Ext { get; set; } //* .htm

        public string Uri { get; set; } //* /123.htm?a=1&b=2
        public string Query { get; set; } //* a=1&b=2

        public string Param { get; set; }

        DataManager _dm;
        public DataManager dm {
            get {
                return this._dm;
            }
        }

        public string mod_path_data { get; set; }

        public Dictionary<string, string> C { get; set; }
        public Dictionary<string, string> Q { get; set; }
        public Dictionary<string, string> P { get; set; }
        public Dictionary<string, FileUpload> F { get; set; }

        public override void Reset()
        {
 	        base.Reset();

            this.mod_path_data = null;

            this.Status = null;

            this.Uri = null;
            this.Query = null;

            this.Ext = null;
            this.Path = null;
            this.FullPath = null;

            this.Param = null;
            this.Unkonwns.Clear();

            this.C.Clear();
            this.Q.Clear();
            this.P.Clear();
            foreach (KeyValuePair<string, FileUpload> kv in this.F) {
                try {
                    string temp_file_name = kv.Value.temp_file_name;
                    if (File.Exists(temp_file_name)) {
                        File.Delete(temp_file_name);
                    }
                } catch {
                }
            }
            this.F.Clear();
        }
    }
}
