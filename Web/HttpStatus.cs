﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vJine.Core.Web
{
    public enum HttpStatus : short
    {
        /// <summary>
        /// 继续
        /// </summary>
        Continue = 100,
        /// <summary>
        /// 转换协议
        /// </summary>
        Switch_Protocols = 101,
        /// <summary>
        /// WebDAV扩展状态码
        /// </summary>
        Processing = 102,
        /// <summary>
        /// OK
        /// </summary>
        OK = 200,
        /// <summary>
        /// 已创建
        /// </summary>
        Created = 201,
        /// <summary>
        /// 接受
        /// </summary>
        Accepted = 202,
        /// <summary>
        /// 非权威信息
        /// </summary>
        Non_Authoritative_Information = 203,
        /// <summary>
        /// 无返回内容
        /// </summary>
        No_Content = 204,
        /// <summary>
        /// 重设内容：如表单
        /// </summary>
        Reset_Content = 205,
        /// <summary>
        /// 部分内容
        /// </summary>
        Partial_Content = 206,
        /// <summary>
        /// WebDAV扩展状态码
        /// </summary>
        Multi_Status = 207,
        /// <summary>
        /// 多个选择
        /// </summary>
        Multiple_Choices = 300,
        /// <summary>
        /// 永久移动
        /// </summary>
        Moved_Permanently = 301,
        /// <summary>
        /// 发现
        /// </summary>
        Found = 302,
        /// <summary>
        /// 见其他
        /// </summary>
        See_Other = 303,
        /// <summary>
        /// 没有被改变
        /// </summary>
        Not_Modified = 304,
        /// <summary>
        /// 使用代理
        /// </summary>
        Use_Proxy = 305,
        /// <summary>
        /// 临时重发
        /// </summary>
        Tempory_Redirect = 307,
        Permanent_Redirect = 308,
        /// <summary>
        /// 坏请求
        /// </summary>
        Bad_Request = 400,
        /// <summary>
        /// 未授权的
        /// </summary>
        Unauthorized = 401,
        /// <summary>
        /// 必要的支付
        /// </summary>
        Payment_Required = 402,
        /// <summary>
        /// 禁用
        /// </summary>
        Forbidden = 403,
        /// <summary>
        /// 没有找到
        /// </summary>
        Not_Found = 404,
        //方式不被允许
        Method_Not_Allowed = 405,
        /// <summary>
        /// 不接受的
        /// </summary>
        Not_Acceptable = 406,
        /// <summary>
        /// 需要代理验证
        /// </summary>
        Proxy_Authentication_Required = 407,
        /// <summary>
        /// 请求超时
        /// </summary>
        Request_Timeout = 408,
        /// <summary>
        /// 冲突
        /// </summary>
        Conflict = 409,
        /// <summary>
        /// 不存在
        /// </summary>
        Gone = 410,
        /// <summary>
        /// 长度必需
        /// </summary>
        Length_Required = 411,
        /// <summary>
        /// 先决条件失败
        /// </summary>
        Pre_Condition_Failed = 412,
        /// <summary>
        /// 请求实体太大
        /// </summary>
        Request_Entity_Too_Large = 413,
        /// <summary>
        /// 请求URI太大
        /// </summary>
        Request_URI_Too_Long = 414,
        /// <summary>
        /// 不被支持的媒体类型
        /// </summary>
        Unsupported_Media_Type = 415,
        /// <summary>
        /// 请求的范围不满足
        /// </summary>
        Requested_Range_Not_Satisfiable = 416,
        /// <summary>
        /// 期望失败
        /// </summary>
        Expectation_Failed = 417,
        /// <summary>
        /// 服务器内部错误
        /// </summary>
        Internal_Server_Error = 500,
        /// <summary>
        /// 不能实现
        /// </summary>
        Not_Implemented = 501,
        /// <summary>
        /// 坏网关
        /// </summary>
        Bad_Gateway = 502,
        /// <summary>
        /// 服务不能获得
        /// </summary>
        Service_Unavailable = 503,
        /// <summary>
        /// 网关超时
        /// </summary>
        Gateway_Timeout = 504,
        /// <summary>
        /// HTTP版本不支持
        /// </summary>
        Http_Version_Not_Support = 505
    }
}
