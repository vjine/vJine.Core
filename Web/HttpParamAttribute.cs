﻿using System;
using System.Collections.Generic;
using System.Text;
using vJine.Core.IoC.Config;

namespace vJine.Core.Web {
    /// <summary>
    /// Cookie
    /// </summary>
    [AttributeUsage(AttributeTargets.Parameter | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class CAttribute : ParamAttribute {
        public CAttribute()
            : this(null, null, null) {
        }

        public CAttribute(string Name)
            : this(Name, null, null) {
        }

        public CAttribute(string Name, string Comments)  {
            this.Name = "C[" + Name + "]";
            this.Comment = Comments;
        }

        public CAttribute(string Name, string[] Patterns, string[] Values) {
            this.Name = "C[" + Name + "]";
            this.IsRequired = true;

            this.Patterns = Patterns;
            this.Values = Values;
        }
    }

    /// <summary>
    /// Query
    /// </summary>
    [AttributeUsage(AttributeTargets.Parameter | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class QAttribute : ParamAttribute {
        public QAttribute()
            : this(null, null, null) {
        }

        public QAttribute(string Name)
            : this(Name, null, null) {
        }

        public QAttribute(string Name, string Comments)  {
            this.Name = "Q[" + Name + "]";
            this.Comment = Comments;
        }

        public QAttribute(string Name, string[] Patterns, string[] Values) {
            this.Name = "Q[" + Name + "]";
            this.IsRequired = true;

            this.Patterns = Patterns;
            this.Values = Values;
        }
    }

    /// <summary>
    /// POST DATA
    /// </summary>
    [AttributeUsage(AttributeTargets.Parameter | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class PAttribute : ParamAttribute {
        public PAttribute()
            : this(null, null, null) {
        }

        public PAttribute(string Name)
            : this(Name, null, null) {
        }

        public PAttribute(string Name, string Comments)  {
            this.Name = "P[" + Name + "]";
            this.Comment = Comments;
        }

        public PAttribute(string Name, string[] Patterns, string[] Values) {
            this.Name = "P[" + Name + "]";
            this.IsRequired = true;

            this.Patterns = Patterns;
            this.Values = Values;
        }
    }

    /// <summary>
    /// POST FILE
    /// </summary>
    [AttributeUsage(AttributeTargets.Parameter | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class FAttribute : ParamAttribute {
        public FAttribute()
            : this(null, null, null) {
        }

        public FAttribute(string Name)
            : this(Name, null, null) {
        }

        public FAttribute(string Name, string Comments)  {
            this.Name = "F[" + Name + "]";
            this.Comment = Comments;
        }

        public FAttribute(string Name, string[] Patterns, string[] Values) {
            this.Name = "F[" + Name + "]";
            this.IsRequired = true;

            this.Patterns = Patterns;
            this.Values = Values;
        }
    }
}
