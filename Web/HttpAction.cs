﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using vJine.Core;
using vJine.Core.Base;
using vJine.Core.IoC;
using vJine.Core.ORM;

namespace vJine.Core.Web
{
    [Serializable]
    [AttributeUsage(AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Property,AllowMultiple=false)]
    public class HttpAction : Attribute
    {
        public HttpAction() {
            this.IsActive = true;
        }

        public HttpAction(string Path)
        {
            this.Path = Path;
            this.IsActive = true;
        }

        [XmlAttribute]
        public string Mod_Name { get; set; }

        [XmlAttribute, Map(IsPrimary = true)]
        public string Class { get; set; }

        [XmlAttribute, Map(IsPrimary = true)]
        public string Name { get; set; }

        [XmlAttribute, Map(UniqueName = "_UN_HttpAction_Path")]
        public string Path { get; set; }
        public partial class _ {
            public static readonly Property<HttpAction, string> Path = new Property<HttpAction, string>("Path");
        }

        public bool AntiXss { get; set; }

        [XmlAttribute, Map(IsNullable = true)]
        public string Comments { get; set; }

        [XmlAttribute, Map(IsNullable = true)]
        public string Init { get; set; }

        [XmlAttribute]
        public bool ReturnRaw { get; set; }

        [XmlAttribute]
        public bool ReturnFile { get; set; }
        [XmlAttribute]
        public bool IsForceShow { get; set; }
        [XmlAttribute]
        public bool IsAttach { get; set; }

        [XmlAttribute, Map(IsNullable = true)]
        public string template { get; set; }

        [XmlAttribute, Map(IsNullable = true)]
        public string Content_Type { get; set; }
        [XmlAttribute, Map(IsNullable = true)]
        public string Content_Encoding { get; set; }

        /// <summary>
        /// 是否需要图片验证码
        /// </summary>
        [XmlAttribute]
        public bool IsVerify { get; set; }
        public partial class _ {
            public static readonly Property<HttpAction, bool> IsVerify = new Property<HttpAction, bool>("IsVerify");
        }

        /// <summary>
        /// 是否激活（可调用）
        /// </summary>
        [XmlAttribute]
        public bool IsActive { get; set; }
        public partial class _ {
            public static readonly Property<HttpAction, bool> IsActive = new Property<HttpAction, bool>("IsActive");
        }

        /// <summary>
        /// 是否需要验证
        /// </summary>
        [XmlAttribute]
        public bool IsLogin { get; set; }
        public partial class _ {
            public static readonly Property<HttpAction, bool> IsLogin = new Property<HttpAction, bool>("IsLogin");
        }

        /// <summary>
        /// 是否需要授权
        /// </summary>
        [XmlAttribute]
        public bool IsAuth { get; set; }
        public partial class _ {
            public static readonly Property<HttpAction, bool> IsAuth = new Property<HttpAction, bool>("IsAuth");
        }

        /// <summary>
        /// 是否记录日志
        /// </summary>
        [XmlAttribute]
        public bool IsLog { get; set; }
        public partial class _ {
            public static readonly Property<HttpAction, bool> IsLog = new Property<HttpAction, bool>("IsLog");
        }

        [Map(IsIgnored=true)]
        public MethodJoint Action { get; set; }
        public object Invoke(object objContext, HttpRequest param, HttpResponse result) {
            //TODO:检查参数合法性
            return this.Action.Invoke(objContext, param, result);
        }

        /// <summary>
        /// 参数中包含文件
        /// </summary>
        //[XmlAttribute]
        public bool has_files { get; set; }
        public bool has_param(string Name, bool is_file = false) {
            if (this.Params == null || this.Params.Count == 0) {
                return false;
            }
            HttpParam httpParam = null;
            for (int i = 0, len = this.Params.Count; i < len; i++) {
                HttpParam param_i = this.Params[i];
                if (param_i.Name == Name) {
                    httpParam = param_i;
                    break;
                }
            }

            if (httpParam == null) {
                return false;
            } else if (is_file && httpParam.Type != "file") {
                return false;
            } else {
                return true;
            }
        }

        [XmlAttribute, Map(IsIgnored = true)]
        public CollectionBase<HttpParam> Params { get; set; }

        public void anti_xss(HttpRequest httpQ) {
            if(this.Params == null || this.Params.Count == 0) {
                return;
            }
            if(!this.AntiXss) {
                return;
            }

            for(int i = 0, len = this.Params.Count; i < len; i++) {
                HttpParam param_i = this.Params[i];
                if(!param_i.AntiXss) {
                    continue;
                }
                string content =
                    Property.Get(httpQ, param_i.Param) as string;
                if(string.IsNullOrEmpty(content)) {
                    continue;
                }

                Property.Set(httpQ, param_i.Param, content);
            }
        }
    }

    [Serializable]
    public class HttpParam {
        [XmlAttribute]
        public string Name { get; set; }

        [XmlAttribute]
        public string Param { get; set; }

        [XmlAttribute]
        public string Type { get; set; }

        public bool AntiXss { get; set; }

        [XmlAttribute, Map(IsNullable=true)]
        public string Comments { get; set; }
    }
}
