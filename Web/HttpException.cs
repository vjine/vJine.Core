﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vJine.Core.Web
{
    public class HttpException : Exception
    {
        public HttpException(Exception ex)
            : base(ex.Message)
        {
        }

        public HttpException(string Msg, params object[] Args)
            : base(string.Format(Msg, Args))
        {

        }
    }
}
