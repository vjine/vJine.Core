﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace vJine.Core.Web
{
    [Serializable]
    public class FileUpload
    {
        [XmlAttribute]
        public string Name { get; set; }
        [XmlAttribute]
        public string Ext { get; set; }
        [XmlAttribute]
        public string Content_Type { get; set; }
        [XmlAttribute]
        public long Size { get; set; }
        [XmlArray]

        [XmlIgnore]
        public string temp_file_name { get; set; }

        public virtual string SaveAs() {
            string file_name = Guid.NewGuid().ToString("D").ToUpper() + this.Ext;
            this.SaveAs(file_name);
            return file_name;
        }
        public virtual void SaveAs(string file_name)
        {
            try {
                File.Move(temp_file_name, file_name);
            } catch (Exception ex) {
                if (File.Exists(temp_file_name)) {
                    File.Delete(temp_file_name);
                }
                throw ex;
            }
        }
    }
}
