﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using vJine.Core.IoC;

namespace vJine.Core.ORM.Adapters {
    /// <summary>
    /// Microsoft SQL Server 数据库适配器类型
    /// </summary>
    public partial class MSSQL : IDbAdapter<MSSQL> {

        static MSSQL() {
            MSSQL.Init_Keywords();
            MSSQL.Init_TypeMap();
        }
        /// <summary>
        /// 实例化Microsoft SQL Server数据库适配器类型
        /// </summary>
        public MSSQL() {
            this.Name = Class<MSSQL>.Name;

            this.ParamPrefix = "@";
            this.GetDateTime = "Select GETDATE()";

            this.QuoteOpen = "[";
            this.QuoteClose = "]";

            this.NULL = " NULL";
            this.NOT_NULL = " NOT NULL";

            this.KeyWords = MSSQL._KeyWords;
        }

        /// <summary>
        /// 生成删除数据库表命令
        /// </summary>
        /// <typeparam name="Tentity">实体类型</typeparam>
        /// <param name="IfExists">判断存在</param>
        /// <param name="table_name">表名</param>
        /// <returns>数据库指令</returns>
        public override DbCommand PrepareDrop<Tentity>(bool IfExists, string table_name) {
            DbCommand dbCmd = this.Conn.CreateCommand();
            dbCmd.CommandType = System.Data.CommandType.Text;
            if (IfExists) {
                dbCmd.CommandText = 
                    "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'"+table_name +"') AND type in (N'U'))" + 
                    "DROP TABLE " + table_name;
            } else {
                dbCmd.CommandText = "DROP TABLE " + table_name;
            }

            return dbCmd;
        }
    }
}
