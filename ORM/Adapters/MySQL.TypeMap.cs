﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using vJine.Core.IoC;

namespace vJine.Core.ORM.Adapters {
    public partial class MySQL : IDbAdapter<MySQL> {
        //http://dev.mysql.com/doc/refman/5.0/en/data-type-overview.html
        //http://www.cnblogs.com/bukudekong/archive/2011/06/27/2091590.html
        static void Init_TypeMap() {
            
            OrmConfig<MySQL>.InitDefaultMap(Reflect.@bool, "BOOL", DbType.Boolean);
            OrmConfig<MySQL>.InitDefaultMap(Reflect.@sbyte, "TINYINT", DbType.SByte, typeof(TypeConverters.sbyte_));
            OrmConfig<MySQL>.InitDefaultMap(Reflect.@byte, "TINYINT UNSIGNED", DbType.Byte);
            OrmConfig<MySQL>.InitDefaultMap(Reflect.@short, "SMALLINT", DbType.Int16);
            OrmConfig<MySQL>.InitDefaultMap(Reflect.@ushort, "SMALLINT UNSIGNED", DbType.UInt16, typeof(TypeConverters.ushort_));
            OrmConfig<MySQL>.InitDefaultMap(Reflect.@int, "INT", DbType.Int32);
            OrmConfig<MySQL>.InitDefaultMap(Reflect.@uint, "INT UNSIGNED", DbType.UInt32, typeof(TypeConverters.uint_));
            OrmConfig<MySQL>.InitDefaultMap(Reflect.@long, "BIGINT", DbType.Int64);
            OrmConfig<MySQL>.InitDefaultMap(Reflect.@ulong, "BIGINT UNSIGNED", DbType.UInt64, typeof(TypeConverters.ulong_));
            OrmConfig<MySQL>.InitDefaultMap(Reflect.@float, "DOUBLE", DbType.Double, typeof(TypeConverters.float_double));
            OrmConfig<MySQL>.InitDefaultMap(Reflect.@double, "DOUBLE", DbType.Double);
            OrmConfig<MySQL>.InitDefaultMap(Reflect.@decimal, "DECIMAL", DbType.Decimal);

            OrmConfig<MySQL>.InitDefaultMap(Reflect.@char, "CHARACTER", DbType.AnsiStringFixedLength, typeof(TypeConverters.char_string));
            OrmConfig<MySQL>.InitDefaultMap(Reflect.@string, "VARCHAR(50)", DbType.AnsiString);
            OrmConfig<MySQL>.InitDefaultMap(Reflect.@DateTime, "DATETIME", DbType.DateTime);

            OrmConfig<MySQL>.InitDefaultMap(Reflect.@byteArray, "BLOB", DbType.Binary,  typeof(TypeConverters.bytes_));

            OrmConfig<MySQL>.InitDefaultMap(typeof(Guid), "VARCHAR(36)", DbType.AnsiString, typeof(TypeConverters.guid_string));
        }
    }
}
