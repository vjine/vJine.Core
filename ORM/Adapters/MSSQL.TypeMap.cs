﻿using System;
using System.Collections.Generic;
using System.Text;
using vJine.Core.IoC;
using System.Data;

namespace vJine.Core.ORM.Adapters {
    public partial class MSSQL : IDbAdapter<MSSQL> {
        // http://msdn.microsoft.com/en-us/library/cc716729.aspx
        static void Init_TypeMap() {

            OrmConfig<MSSQL>.InitDefaultMap(Reflect.@bool, "BIT", DbType.Boolean);
            OrmConfig<MSSQL>.InitDefaultMap(Reflect.@sbyte, "TINYINT", DbType.Byte, typeof(TypeConverters.sbyte_byte));
            OrmConfig<MSSQL>.InitDefaultMap(Reflect.@byte, "TINYINT", DbType.Byte);
            OrmConfig<MSSQL>.InitDefaultMap(Reflect.@short, "SMALLINT", DbType.Int16);
            OrmConfig<MSSQL>.InitDefaultMap(Reflect.@ushort, "SMALLINT", DbType.Int16, typeof(TypeConverters.ushort_short));
            OrmConfig<MSSQL>.InitDefaultMap(Reflect.@int, "INT", DbType.Int32);
            OrmConfig<MSSQL>.InitDefaultMap(Reflect.@uint, "INT", DbType.Int32, typeof(TypeConverters.uint_int));
            OrmConfig<MSSQL>.InitDefaultMap(Reflect.@long, "BIGINT", DbType.Int64);
            OrmConfig<MSSQL>.InitDefaultMap(Reflect.@ulong, "BIGINT", DbType.Int64, typeof(TypeConverters.ulong_long));
            OrmConfig<MSSQL>.InitDefaultMap(Reflect.@float, "REAL", DbType.Single);
            OrmConfig<MSSQL>.InitDefaultMap(Reflect.@double, "FLOAT", DbType.Double);
            OrmConfig<MSSQL>.InitDefaultMap(Reflect.@decimal, "DECIMAL", DbType.Decimal);

            OrmConfig<MSSQL>.InitDefaultMap(Reflect.@char, "CHAR(1)", DbType.AnsiStringFixedLength, typeof(TypeConverters.char_string));
            OrmConfig<MSSQL>.InitDefaultMap(Reflect.@string, "VARCHAR(200)", DbType.AnsiString);
            OrmConfig<MSSQL>.InitDefaultMap(Reflect.@DateTime, "DATETIME", DbType.DateTime);

            OrmConfig<MSSQL>.InitDefaultMap(Reflect.@byteArray, "IMAGE", DbType.Binary, typeof(TypeConverters.bytes_));

            OrmConfig<MSSQL>.InitDefaultMap(typeof(Guid), "UNIQUEIDENTIFIER", DbType.Guid);
        }
    }
}
