﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;

using vJine.Core.IO;
using vJine.Core.IO.Json;
using vJine.Core.IoC;
using vJine.Core.IoC.Config;

namespace vJine.Core.ORM {
    public partial class Class<Tentity> {
        public static readonly string Name;
        public static readonly string FullName;
        public static readonly string AssemblyQualifiedName;
        public static readonly Type type;

        public static Property<Tentity, bool>[] ActiveFlags { get; protected set; }
        public static Class<Tentity>.Property[] Keys { get; protected set; } //Keys:Dor Dictionary

        public static readonly InitJoint<Tentity> Ctor = null;

        public static implicit operator Class<Tentity>.Property[](Class<Tentity> C) {
            return Class<Tentity>.Columns;
        }

        public static implicit operator Class<Tentity>.Property(Class<Tentity> C) {
            Property[] P = Class<Tentity>.Columns; Property p = null;

            for (int i = 0, len = P.Length; i < len; i++) {
                if (p == null) {
                    p = P[i]; continue;
                }

                p = new Property(p, ",", P[i]);
            }

            return p;
        }

        static Dictionary<string, Property> Properties = new Dictionary<string, Property>();
        static void Init_Properties() {
            PropertyInfo[] P =
                Reflect.GetProperties<Tentity>(BindingFlags.Public | BindingFlags.Instance);

            for (int i = 0, len = P.Length; i < len; i++) {
                PropertyInfo p = P[i];
                string pName = p.Name;

                ParameterInfo[] pis = p.GetIndexParameters();
                if (pis != null && pis.Length > 0) { //Ignore Item(Index),Count...
                    continue;
                }
                Class<Tentity>.Properties.Add(pName, new Property(p/*, i*/));
            }

            Class<Tentity>.Columns = new Property[Class<Tentity>.Properties.Count];
            Properties.Values.CopyTo(Columns, 0);
        }

        static Class() {
            Type Tclass = typeof(Tentity); Class<Tentity>.type = Tclass;

            Class<Tentity>.AssemblyQualifiedName = Tclass.AssemblyQualifiedName;
            Class<Tentity>.FullName =
                Regex.Replace(Class<Tentity>.AssemblyQualifiedName, @", Version=[^\]]+", "", RegexOptions.IgnoreCase);

            ClassAttribute[] C = Reflect.GetAttribute<ClassAttribute>(Tclass);
            if (C == null || C.Length == 0) {
                Class<Tentity>.Name = Tclass.Name;
            } else {
                Class<Tentity>.Name = C[0].Name;
                if (string.IsNullOrEmpty(Class<Tentity>.Name)) {
                    Class<Tentity>.Name = Tclass.Name;
                }
            }

            Init_Properties();

            Property[] P = Class<Tentity>.GetMap();

            Type T_ActiveFlag = typeof(ActiveFlagAttribute);

            List<Property<Tentity, bool>> _ActiveFlags = new List<Property<Tentity, bool>>();

            List<Property> _Keys = new List<Property>();
            Dictionary<string,List<Class<Tentity>.Property>> _Uniques = new Dictionary<string,List<Property>>();
            List<Property> _Values = new List<Property>();

            for (int i = 0, len = P.Length; i < len; i++) {
                Property p_i = P[i];
                MapAttribute[] maps = Reflect.GetAttribute<MapAttribute>(p_i);
                if(maps != null && maps.Length > 0) {
                    if(maps[0].IsIgnored) {
                        continue;
                    }
                }

                if (p_i.IsBool && Reflect.HasAttribute(p_i, T_ActiveFlag, true)) {
                    _ActiveFlags.Add(new Property<Tentity, bool>(p_i.Name));
                }

                MapAttribute[] tags = Reflect.GetAttribute<MapAttribute>(p_i);

                int len_tag = tags.Length;
                //Keys:For Dictionary
                for (int i_tag = 0; i_tag < len_tag; i_tag++) {
                    MapAttribute tag_i = tags[i_tag];
                    if (tag_i.IsPrimary) {
                        _Keys.Add(p_i);
                        break;
                    }
                }
                Class<Tentity>.Keys = _Keys.ToArray();
            }

            Class<Tentity>.ActiveFlags = _ActiveFlags.ToArray();

            Class<Tentity>.Ctor = new InitJoint<Tentity>();

            //Class<T>.defaultComparer = Emit.Comparer<T>();
        }

        public static InitSignature<Tentity> Init(params Type[] Tparams) {
            return Emit.GenCtor<Tentity>(Tparams);
        }

        static Property[] Columns = null;

        public static Property[] GetMap(params string[] Names) {
            if (Names == null || Names.Length == 0) {
                return Columns;
            }

            List<Property> C = new List<Property>();
            for (int i = 0, len = Names.Length; i < len; i++) {
                string Name = Names[i];
                if (!Class<Tentity>.Properties.ContainsKey(Name)) {
                    continue;
                }

                C.Add(Class<Tentity>.Properties[Name]);
            }

            return C.ToArray();
        }

        public static Property[] GetExtraMap(params string[] P) {
            List<Property> C = new List<Property>();

            foreach (KeyValuePair<string, Property> kv in Properties) {
                bool Exclude = false; string Name = kv.Key;
                for (int j = 0, len_j = P.Length; j < len_j; j++) {
                    if (Name == P[j]) {
                        Exclude = true;
                        break;
                    }
                }
                if (Exclude) {
                    continue;
                }

                C.Add(kv.Value);
            }

            return C.ToArray();
        }

        public static Property GetProperty(string Name) {
            Property[] P = Class<Tentity>.GetMap(Name);
            if (P == null || P.Length == 0) {
                return null;
            }

            return P[0];
        }

        public static Property[] GetMap(DbDataReader reader) {
            List<Property> C = new List<Property>();

            for (int i = 0, len = reader.FieldCount; i < len; i++) {
                string fName = reader.GetName(i);
                if (!Properties.ContainsKey(fName)) {
                    throw new CoreException("Field not found:" + fName);
                }

                C.Add(Properties[fName]);
            }

            return C.ToArray();
        }
    }

    //TODO:Instance Manager
    public partial class Class<Tentity> {
        public static void Prepare(int capacity) {

        }

        public static Tentity Create() {
            InitJoint<Tentity> ctor = Class<Tentity>.Ctor;
            if (ctor == null) {
                throw new CoreException("类【{0}】缺少无参构造函数", Class<Tentity>.FullName);
            }
            return ctor.Invoke(null);
        }

        public static void Collect(Tentity obj) {

        }
    }

    //ToString & Parse
    public partial class Class<Tentity> {
        public static string ToString(Tentity jsonObject) {
            return JsonHelper<Tentity>.ToString(jsonObject);
        }

        public static void ToString(Tentity jsonObject, Stream jsonStream) {
            JsonHelper<Tentity>.ToString(jsonObject, jsonStream);
        }

        public static Tentity Parse(string jsonString) {
            return JsonHelper.Parse<Tentity>(jsonString);
        }

        public static Tentity Parse(Stream jsonStream) {
            return JsonHelper.Parse<Tentity>(jsonStream);
        }

        public static void Parse(Stream jsonStream, Tentity jsonObject) {
            JsonHelper.Parse<Tentity>(jsonStream, jsonObject);
        }
    }

    public partial class Class<Tentity> {
        static readonly Call<int, Tentity, Tentity> defaultComparer = null;
        public static void Sort(IList<Tentity> data) {
            if (Class<Tentity>.defaultComparer == null) {
                Class<Tentity>.Sort(data, Emit.Comparer<Tentity>(Class<Tentity>.GetMap())); return;
                //throw new CoreException("default Comparer Is Null:{0}", Class<T>.FullName);
            }

            Class<Tentity>.Sort(data, Class<Tentity>.defaultComparer);
        }

        public static void Sort(IList<Tentity> data, params Class<Tentity>.Property[] P) {
            Class<Tentity>.Sort(data, Emit.Comparer<Tentity>(P));
        }

        public static void Sort(IList<Tentity> data, Call<int, Tentity, Tentity> comparer) {

            int min = 0, max = 0, mid = 0;
            int r_compare = 0, index_prev = 0;
            Tentity data_i = default(Tentity), data_prev = default(Tentity), data_mid = default(Tentity);
            
            for (int i = 1, len = data.Count; i < len; i++) {

                data_i = data[i];
                
                if (i == 1) {
                    min = 0; max = i - 1;
                } else {
                    if (comparer(data_i, data_prev) >= 0) {
                        min = index_prev; max = i - 1;
                    } else {
                        min = 0;  max = index_prev;
                    }
                }

                do {
                    mid = (min + max) / 2; data_mid = data[mid];
                    r_compare = comparer(data_i, data_mid);

                    if (r_compare > 0) {
                        min = mid + 1;
                    } else if (r_compare < 0) {
                        max = mid - 1;
                    } else {
                        break;
                    }
                } while (min <= max);


                index_prev = r_compare > 0 ? mid + 1 : mid;
                data_prev = data_i;

                data.RemoveAt(i);
                data.Insert(index_prev, data_i);
            }
        }

        public static string CheckSort(IList<Tentity> data) {
            return Class<Tentity>.CheckSort(data, null);
        }

        static string checkPropName = null; static readonly object checkLock = new object();
        public static string CheckSort(IList<Tentity> data, params Class<Tentity>.Property[] P) {
            lock (Class<Tentity>.checkLock) {
                Call<int, Tentity, Tentity> comparer =
                    Emit.Comparer<Tentity>((string name, int r, object arg1, object arg2) => {
                        Class<Tentity>.checkPropName /**/ = name;
                        //TODO:内部变量不可以在代理中引用，程序报公共语言运行时错误
                    }, P);

                for (int i = 1, len = data.Count; i < len; i++) {
                    if (comparer(data[i - 1], data[i]) > 0) {
                        return string.Format("Name:{0},{1} - {2}", Class<Tentity>.checkPropName, i - 1, i);
                    }
                }

                return null;
            }
        }
    }

    //Property
    public partial class Class<Tentity> {
        public class Property : vJine.Core.IoC.Property {
            public Property(string name)
                : base(Class<Tentity>.GetProperty(name)) {
            }

            public static implicit operator Property[](Property p) {
                return new Property[] { p };
            }

            public static implicit operator Property(Property[] P) {
                if (P == null || P.Length != 1) {
                    throw new CoreException(MethodBase.GetCurrentMethod(), "Fail To Convert");
                }

                return P[0];
            }

            internal Property(object P1, string Op, object P2)
                : base(P1, Op, P2) {
            }

            internal Property(PropertyInfo p/*, int index*/)
                : base(p/*, index*/) {
            }

            internal Property(Property P)
                : base(P) {
            }

            public override string ToString() {
                return this.Name;
            }
        }
    }

    //Set
    public partial class Class<Tentity> {
        public class Set : Class<Tentity>.Property {

            public static Set operator &(Set S1, Set S2) {
                if (S1 == null && S2 == null) {
                    return null;
                } else if (S1 == null) {
                    return S2;
                } else if (S2 == null) {
                    return S1;
                } else {
                    return new Set(S1, ",", S2);
                }
            }

            internal Set(object L, string Op, object R)
                : base(L, Op, R) {
            }
        }
    }

    //Where
    public partial class Class<Tentity> {
        public class Where : Class<Tentity>.Property {
            public class _ {
                public static readonly Property<Where, object> L = new Property<Where, object>("L");
                public static readonly Property<Where, string> Op = new Property<Where, string>("Op");
                public static readonly Property<Where, object> R = new Property<Where, object>("R");
            }

            public static Where operator &(Where W1, Where W2) {
                if (W1 == null && W2 == null) {
                    return null;
                } else if (W1 == null) {
                    return W2;
                } else if (W2 == null) {
                    return W1;
                } else {
                    return new Where(W1, "and", W2);
                }
            }

            public static Where operator |(Where W1, Where W2) {
                if (W1 == null && W2 == null) {
                    return null;
                } else if (W1 == null) {
                    return W2;
                } else if (W2 == null) {
                    return W1;
                } else {
                    return new Where(W1, "or", W2);
                }
            }

            internal Where(Property P)
                : base(P) {
            }

            internal Where(object L, string Op, object R)
                : base(L, Op, R) {
            }
        }
    }
    /// <summary>
    /// 属性辅助类
    /// </summary>
    /// <typeparam name="Tentity">实体类型</typeparam>
    /// <typeparam name="Tproperty">属性类型</typeparam>
    public partial class Property<Tentity, Tproperty> : Class<Tentity>.Property {
        GetSignature<Tentity, Tproperty> getter;
        SetSignature<Tentity, Tproperty> setter;
        /// <summary>
        /// 以指定的属性实例化属性类型
        /// </summary>
        /// <param name="P">属性</param>
        public Property(Class<Tentity>.Property P)
            : base(P) {
            this.Init_Property();
        }
        /// <summary>
        /// 以指定的属性名实例化属性类型
        /// </summary>
        /// <param name="Name">属性名称</param>
        public Property(string Name)
            : base(Class<Tentity>.GetMap(Name)) {
            this.Init_Property();
        }
        /// <summary>
        /// 以指定的属性名实例化属性类型
        /// </summary>
        /// <param name="Name">属性名称</param>
        /// <param name="init_Prop_Helper">是否实例化属性访问代理</param>
        public Property(string Name, bool init_Prop_Helper)
            : base(Class<Tentity>.GetMap(Name)) {
            this.Init_Property(init_Prop_Helper);
        }

        internal Property(object L, string Op, object R)
            : base(L, Op, R) {
        }

        void Init_Property() {
            this.Init_Property(true);
        }

        void Init_Property(bool init_helper) {
            if (init_helper && getter == null) {
                getter = Property<Tentity, Tproperty>.Get(this);
                setter = Property<Tentity, Tproperty>.Set(this);
            }
        }
        /// <summary>
        /// 获取指定属性的Get代理
        /// </summary>
        /// <param name="P">属性</param>
        /// <returns>Get代理</returns>
        public static GetSignature<Tentity, Tproperty> Get(Class<Tentity>.Property P) {
            return Emit.GenGetter<Tentity, Tproperty>(P);
        }
        /// <summary>
        /// 获取指定属性的Get代理
        /// </summary>
        /// <param name="Name">属性名称</param>
        /// <returns>Get代理</returns>
        public static GetSignature<Tentity, Tproperty> Get(string Name) {
            return Emit.GenGetter<Tentity, Tproperty>(Name);
        }
        /// <summary>
        /// 获取指定属性的Set代理
        /// </summary>
        /// <param name="P">属性</param>
        /// <returns>Set代理</returns>
        public static SetSignature<Tentity, Tproperty> Set(Class<Tentity>.Property P) {
            return Emit.GenSetter<Tentity, Tproperty>(P);
        }
        /// <summary>
        /// 获取指定属性的Set代理
        /// </summary>
        /// <param name="Name">属性名称</param>
        /// <returns>Set代理</returns>
        public static SetSignature<Tentity, Tproperty> Set(string Name) {
            return Emit.GenSetter<Tentity, Tproperty>(Name);
        }
        /// <summary>
        /// 获取属性值
        /// </summary>
        /// <param name="entity">实体实例</param>
        /// <returns>属性值</returns>
        public Tproperty Get(Tentity entity) {
            return getter(entity);
        }
        /// <summary>
        /// 设置属性值
        /// </summary>
        /// <param name="entity">实体实例</param>
        /// <param name="v">属性值</param>
        public void Set(Tentity entity, Tproperty v) {
            setter(entity, v);
        }
    }

    public partial class Property<Tentity, Tproperty> : Class<Tentity>.Property {
        /// <summary>
        /// 更新表达式：等于
        /// </summary>
        /// <param name="P">属性表达式</param>
        /// <returns>更新表达式</returns>
        public Class<Tentity>.Set EQ(Class<Tentity>.Where P) {
            return new Class<Tentity>.Set(this, "=", P);
        }
        /// <summary>
        /// 更新表达式：等于
        /// </summary>
        /// <param name="P">属性</param>
        /// <returns>更新表达式</returns>
        public Class<Tentity>.Set EQ(Property<Tentity, Tproperty> P) {
            return new Class<Tentity>.Set(this, "=", P);
        }
        /// <summary>
        /// 更新表达式：等于
        /// </summary>
        /// <param name="V">属性值</param>
        /// <returns>更新表达式</returns>
        public Class<Tentity>.Set EQ(Tproperty V) {
            return new Class<Tentity>.Set(this, "=", V);
        }
        /// <summary>
        /// 条件表达式：相似
        /// </summary>
        /// <param name="V">值</param>
        /// <returns>条件表达式</returns>
        public Class<Tentity>.Where Like(string V) {
            if (V == null) {
                throw new ArgumentNullException("V");
            }
            if (!Reflect.IsString(this.pType)) {
                throw new ArgumentException("类型不匹配：值必须为字符串", "V");
            }

            return new Class<Tentity>.Where(this, "like", V);
        }
        /// <summary>
        /// 条件表达式：不相似
        /// </summary>
        /// <param name="V">值</param>
        /// <returns>条件表达式</returns>
        public Class<Tentity>.Where NotLike(string V) {
            if (V == null) {
                throw new ArgumentNullException("V");
            }
            if (!Reflect.IsString(this.pType)) {
                throw new ArgumentException("类型不匹配：值必须为字符串", "V");
            }

            return new Class<Tentity>.Where(this, "not like", V);
        }
        /// <summary>
        /// 条件表达式：可空
        /// </summary>
        /// <returns>条件表达式</returns>
        public Class<Tentity>.Where IsNull() {
            bool isNullable = 
                this.IsNullable || (this.Map != null && this.Map.IsNullable);
            if (!isNullable) {
                throw new CoreException("属性类型不可空[{0}]", this.Name);
            }

            return new Class<Tentity>.Where(this, "is", "null");
        }
        /// <summary>
        /// 条件表达式：不可空
        /// </summary>
        /// <returns>条件表达式</returns>
        public Class<Tentity>.Where IsNotNull() {
            bool isNullable =
                this.IsNullable || (this.Map != null && this.Map.IsNullable);
            if(!isNullable) {
                throw new CoreException("属性类型不可空[{0}]", this.Name);
            }

            return new Class<Tentity>.Where(this, "is", "not null");
        }
        /// <summary>
        /// 字段排序：升序
        /// </summary>
        /// <returns>属性</returns>
        public Class<Tentity>.Property ASC() {
            return new Class<Tentity>.Property(this) { IsASC = true };
        }
        /// <summary>
        /// 字段排序：降序
        /// </summary>
        /// <returns>属性</returns>
        public Class<Tentity>.Property DESC() {
            return new Class<Tentity>.Property(this) { IsASC = false };
        }

        #region Operators
        /// <summary>
        /// 操作符：与
        /// </summary>
        /// <param name="C1">属性1</param>
        /// <param name="C2">属性2</param>
        /// <returns>条件表达式</returns>
        public static Class<Tentity>.Where operator &(Property<Tentity, Tproperty> C1, Class<Tentity>.Property C2) {
            return new Class<Tentity>.Where(C1, "and", C2);
        }
        /// <summary>
        /// 操作符：或
        /// </summary>
        /// <param name="C1">属性1</param>
        /// <param name="C2">属性2</param>
        /// <returns>条件表达式</returns>
        public static Class<Tentity>.Where operator |(Property<Tentity, Tproperty> C1, Class<Tentity>.Property C2) {
            return new Class<Tentity>.Where(C1, "or", C2);
        }
        /// <summary>
        /// 操作符：相等
        /// </summary>
        /// <param name="C1">属性1</param>
        /// <param name="C2">属性2</param>
        /// <returns>条件表达式</returns>
        public static Class<Tentity>.Where operator ==(Property<Tentity, Tproperty> C1, Property<Tentity, Tproperty> C2) {
            return new Class<Tentity>.Where(C1, "=", C2);
        }
        /// <summary>
        /// 操作符：相等
        /// </summary>
        /// <param name="C1">属性1</param>
        /// <param name="V2">数值2</param>
        /// <returns>条件表达式</returns>
        public static Class<Tentity>.Where operator ==(Property<Tentity, Tproperty> C1, Tproperty V2) {
            return new Class<Tentity>.Where(C1, "=", V2);
        }
        /// <summary>
        /// 操作符：不相等
        /// </summary>
        /// <param name="C1">属性1</param>
        /// <param name="C2">属性2</param>
        /// <returns>条件表达式</returns>
        public static Class<Tentity>.Where operator !=(Property<Tentity, Tproperty> C1, Property<Tentity, Tproperty> C2) {
            return new Class<Tentity>.Where(C1, "!=", C2);
        }
        /// <summary>
        /// 操作符：不相等
        /// </summary>
        /// <param name="C1">属性1</param>
        /// <param name="V2">数值2</param>
        /// <returns>条件表达式</returns>
        public static Class<Tentity>.Where operator !=(Property<Tentity, Tproperty> C1, Tproperty V2) {
            return new Class<Tentity>.Where(C1, "!=", V2);
        }
        /// <summary>
        /// 操作符：大于
        /// </summary>
        /// <param name="C1">属性1</param>
        /// <param name="C2">属性2</param>
        /// <returns>条件表达式</returns>
        public static Class<Tentity>.Where operator >(Property<Tentity, Tproperty> C1, Property<Tentity, Tproperty> C2) {
            return new Class<Tentity>.Where(C1, ">", C2);
        }
        /// <summary>
        /// 操作符：大于
        /// </summary>
        /// <param name="C1">属性1</param>
        /// <param name="V2">数值2</param>
        /// <returns>条件表达式</returns>
        public static Class<Tentity>.Where operator >(Property<Tentity, Tproperty> C1, Tproperty V2) {
            return new Class<Tentity>.Where(C1, ">", V2);
        }
        /// <summary>
        /// 操作符：大于等于
        /// </summary>
        /// <param name="C1">属性1</param>
        /// <param name="C2">属性2</param>
        /// <returns>条件表达式</returns>
        public static Class<Tentity>.Where operator >=(Property<Tentity, Tproperty> C1, Property<Tentity, Tproperty> C2) {
            return new Class<Tentity>.Where(C1, ">=", C2);
        }
        /// <summary>
        /// 操作符：大于等于
        /// </summary>
        /// <param name="C1">属性1</param>
        /// <param name="V2">数值2</param>
        /// <returns>条件表达式</returns>
        public static Class<Tentity>.Where operator >=(Property<Tentity, Tproperty> C1, Tproperty V2) {
            return new Class<Tentity>.Where(C1, ">=", V2);
        }
        /// <summary>
        /// 操作符：小于
        /// </summary>
        /// <param name="C1">属性1</param>
        /// <param name="C2">属性2</param>
        /// <returns>条件表达式</returns>
        public static Class<Tentity>.Where operator <(Property<Tentity, Tproperty> C1, Property<Tentity, Tproperty> C2) {
            return new Class<Tentity>.Where(C1, "<", C2);
        }
        /// <summary>
        /// 操作符：小于
        /// </summary>
        /// <param name="C1">属性1</param>
        /// <param name="V2">数值2</param>
        /// <returns>条件表达式</returns>
        public static Class<Tentity>.Where operator <(Property<Tentity, Tproperty> C1, Tproperty V2) {
            return new Class<Tentity>.Where(C1, "<", V2);
        }
        /// <summary>
        /// 操作符：小于等于
        /// </summary>
        /// <param name="C1">属性1</param>
        /// <param name="C2">属性2</param>
        /// <returns>条件表达式</returns>
        public static Class<Tentity>.Where operator <=(Property<Tentity, Tproperty> C1, Property<Tentity, Tproperty> C2) {
            return new Class<Tentity>.Where(C1, "<=", C2);
        }
        /// <summary>
        /// 操作符：小于等于
        /// </summary>
        /// <param name="C1">属性1</param>
        /// <param name="V2">数值2</param>
        /// <returns>条件表达式</returns>
        public static Class<Tentity>.Where operator <=(Property<Tentity, Tproperty> C1, Tproperty V2) {
            return new Class<Tentity>.Where(C1, "<=", V2);
        }
        /// <summary>
        /// 操作符：加
        /// </summary>
        /// <param name="C1">属性1</param>
        /// <param name="C2">属性2</param>
        /// <returns>条件表达式</returns>
        public static Class<Tentity>.Where operator +(Property<Tentity, Tproperty> C1, Property<Tentity, Tproperty> C2) {
            return new Class<Tentity>.Where(C1, "+", C2);
        }
        /// <summary>
        /// 操作符：加
        /// </summary>
        /// <param name="C1">属性1</param>
        /// <param name="V2">数值2</param>
        /// <returns>条件表达式</returns>
        public static Property<Tentity, Tproperty> operator +(Property<Tentity, Tproperty> C1, Tproperty V2) {
            return new Property<Tentity, Tproperty>(C1, "+", V2);
        }
        /// <summary>
        /// 操作符：减
        /// </summary>
        /// <param name="C1">属性1</param>
        /// <param name="C2">属性2</param>
        /// <returns>条件表达式</returns>
        public static Property<Tentity, Tproperty> operator -(Property<Tentity, Tproperty> C1, Property<Tentity, Tproperty> C2) {
            return new Property<Tentity, Tproperty>(C1, "-", C2);
        }
        /// <summary>
        /// 操作符：减
        /// </summary>
        /// <param name="C1">属性1</param>
        /// <param name="V2">数值2</param>
        /// <returns>条件表达式</returns>
        public static Property<Tentity, Tproperty> operator -(Property<Tentity, Tproperty> C1, Tproperty V2) {
            return new Property<Tentity, Tproperty>(C1, "-", V2);
        }
        /// <summary>
        /// 操作符：乘
        /// </summary>
        /// <param name="C1">属性1</param>
        /// <param name="C2">属性2</param>
        /// <returns>条件表达式</returns>
        public static Property<Tentity, Tproperty> operator *(Property<Tentity, Tproperty> C1, Property<Tentity, Tproperty> C2) {
            return new Property<Tentity, Tproperty>(C1, "*", C2);
        }
        /// <summary>
        /// 操作符：乘
        /// </summary>
        /// <param name="C1">属性1</param>
        /// <param name="V2">数值2</param>
        /// <returns>条件表达式</returns>
        public static Property<Tentity, Tproperty> operator *(Property<Tentity, Tproperty> C1, Tproperty V2) {
            return new Property<Tentity, Tproperty>(C1, "*", V2);
        }
        /// <summary>
        /// 操作符：除
        /// </summary>
        /// <param name="C1">属性1</param>
        /// <param name="C2">属性2</param>
        /// <returns>条件表达式</returns>
        public static Property<Tentity, Tproperty> operator /(Property<Tentity, Tproperty> C1, Property<Tentity, Tproperty> C2) {
            return new Property<Tentity, Tproperty>(C1, "/", C2);
        }
        /// <summary>
        /// 操作符：除
        /// </summary>
        /// <param name="C1">属性1</param>
        /// <param name="V2">数值2</param>
        /// <returns>条件表达式</returns>
        public static Property<Tentity, Tproperty> operator /(Property<Tentity, Tproperty> C1, Tproperty V2) {
            return new Property<Tentity, Tproperty>(C1, "/", V2);
        }
        /// <summary>
        /// 操作符：求余
        /// </summary>
        /// <param name="C1">属性1</param>
        /// <param name="C2">属性2</param>
        /// <returns>条件表达式</returns>
        public static Property<Tentity, Tproperty> operator %(Property<Tentity, Tproperty> C1, Property<Tentity, Tproperty> C2) {
            return new Property<Tentity, Tproperty>(C1, "%", C2);
        }
        /// <summary>
        /// 操作符：求余
        /// </summary>
        /// <param name="C1">属性1</param>
        /// <param name="V2">数值2</param>
        /// <returns>条件表达式</returns>
        public static Property<Tentity, Tproperty> operator %(Property<Tentity, Tproperty> C1, Tproperty V2) {
            return new Property<Tentity, Tproperty>(C1, "%", V2);
        }
        #endregion Operators
        public override bool Equals(object obj) {
            return base.Equals(obj);
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }
    }
}
