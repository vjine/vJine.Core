﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;

namespace vJine.Core.ORM {
    /// <summary>
    /// 数据库命令接口
    /// </summary>
    public interface IDbCmd {
        /// <summary>
        /// 预生成数据库命令
        /// </summary>
        /// <param name="adapter">数据库适配器</param>
        /// <returns>数据库命令</returns>
        DbCommand Prepare(IDbAdapter adapter);
        /// <summary>
        /// 是否已经预生成
        /// </summary>
        bool IsPrepared { get; set; }
    }
}
