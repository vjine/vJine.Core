﻿using System;
using System.Collections.Generic;
using System.Text;
using vJine.Core.Base;

namespace vJine.Core.ORM {
    public interface IConverter<T> {
        object CONV_I(object V);
        T CONV_Q(object V);
    }

    public class TypeConverters {

        public class bool_string /*: IConverter<bool>*/ {
            public static object CONV_I(bool V) {
                if((bool)V) {
                    return "1";
                } else {
                    return "0";
                }
            }

            public static bool CONV_Q(string V) {
                if((string)V == "1") {
                    return true;
                } else {
                    return false;
                }
            }
        }

        public class sbyte_ /*: IConverter<sbyte>*/ {
            public static sbyte CONV_Q(object V) {
                return (sbyte)V;
            }
        }

        public class sbyte_byte /*: IConverter<sbyte>*/ {

            public static object CONV_I(sbyte V) {
                return (byte)V;
            }

            public static sbyte CONV_Q(byte V) {
                return (sbyte)V;
            }
        }

        public class sbyte_short /*: IConverter<sbyte>*/ {

            public static object CONV_I(sbyte V) {
                return (short)V;
            }

            public static sbyte CONV_Q(short V) {
                return (sbyte)V;
            }
        }

        public class byte_short /*: IConverter<byte>*/ {
            public static object CONV_I(byte V, string name) {
                return (short)V;
            }

            public static byte CONV_Q(short V) {
                return (byte)V;
            }
        }

        public class short_int /*: IConverter<short>*/ {
            public static object CONV_I(short V) {
                return (int)V;
            }

            public static short CONV_Q(int V) {
                return (short)V;
            }
        }

        public class ushort_ /*: IConverter<ushort>*/ {
            public static ushort CONV_Q(object V) {
                return (ushort)V;
            }
        }

        public class ushort_short /*: IConverter<ushort>*/ {
            public static object CONV_I(ushort V) {
                return (short)V;
            }

            public static ushort CONV_Q(short V) {
                return (ushort)V;
            }
        }

        public class ushort_int /*: IConverter<ushort>*/ {
            public static object CONV_I(ushort V) {
                return (int)V;
            }

            public static ushort CONV_Q(int V) {
                return (ushort)V;
            }
        }

        public class int_long /*: IConverter<int>*/ {
            public static object CONV_I(int V) {
                return (long)V;
            }

            public static int CONV_Q(long V) {
                return Convert.ToInt32(V);
            }
        }

        public class uint_ /*: IConverter<uint>*/ {
            public static uint CONV_Q(object V) {
                return (uint)V;
            }
        }

        public class uint_int /*: IConverter<ulong>*/ {
            public static object CONV_I(uint V) {
                return (int)V;
            }

            public static uint CONV_Q(int V) {
                return (uint)V;
            }
        }

        public class uint_long /*: IConverter<uint>*/ {
            public static object CONV_I(uint V) {
                return (long)V;
            }

            public static uint CONV_Q(long V) {
                return (uint)V;
            }
        }

        public class long_decimal /*: IConverter<long>*/ {
            public static object CONV_I(long V) {
                return (decimal)V;
            }

            public static long CONV_Q(decimal V) {
                return (long)V;
            }
        }

        public class ulong_ /*: IConverter<ulong>*/ {
            public static ulong CONV_Q(object V) {
                return (ulong)V;
            }
        }

        public class ulong_long /*: IConverter<ulong>*/ {
            public static object CONV_I(ulong V) {
                return (long)V;
            }

            public static ulong CONV_Q(long V) {
                return (ulong)V;
            }
        }

        public class ulong_decimal /*: IConverter<ulong>*/ {
            public static object CONV_I(ulong V) {
                return (decimal)V;
            }

            public static ulong CONV_Q(decimal V) {
                return (ulong)V;
            }
        }       

        public class float_double /*: IConverter<float>*/ {
            public static object CONV_I(float V) {
                return Convert.ToDouble(V);
            }

            public static float CONV_Q(double V) {
                return (float)V;
            }
        }

        public class char_string /*: IConverter<char>*/ {
            public static object CONV_I(object V) {
                if (V is Char) {
                    return V;
                } else if (V is string) {
                    string chars = (string)V;
                    if (chars == "") {
                        return '\0';
                    } else {
                        return chars[0];
                    }
                } else {
                    throw new OrmException("Fail To Convert String[{0}] To Char", V);
                }
            }

            public static char CONV_Q(string V) {
                return V[0];
            }
        }

        public class datetime_string /*: IConverter<DateTime>*/ {
            static string date_format = "yyyy-MM-dd HH:mm:ss.fff";
            public static object CONV_I(DateTime V) {
                return V.ToString(date_format);
            }

            public static DateTime CONV_Q(string V) {
                DateTime parse_result = DateTime.Now;
                if(!DateTime.TryParseExact(V, date_format, null, System.Globalization.DateTimeStyles.AssumeLocal, out parse_result)) {
                    throw new CoreException("字符串【{0}】无法按限定的格式【{1}】解析", V, date_format);
                }
                return parse_result;
            }
        }

        public class guid_string /*: IConverter<Guid>*/ {
            public static object CONV_I(Guid V) {
                return ((Guid)V).ToString();
            }

            public static Guid CONV_Q(string V) {
                return new Guid(V as string);
            }
        }

        public class bytes_ /*: IConverter<byte[]>*/ {
            //public static object CONV_I(object V) {
            //    return V.ToString();
            //}

            public static byte[] CONV_Q(object V) {
                return V as byte[];
            }
        } 
    }
}
