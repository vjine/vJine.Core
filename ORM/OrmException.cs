﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace vJine.Core.ORM {
    /// <summary>
    /// 表示在调用ORM期间发生的异常
    /// </summary>
    public class OrmException : CoreException {
        /// <summary>
        /// 实例化ORM异常
        /// </summary>
        public OrmException()
            : base() {

        }
        /// <summary>
        /// 实例化ORM异常
        /// </summary>
        /// <param name="ex">导致当前异常的异常</param>
        public OrmException(Exception ex)
            : base(ex) {

        }
        /// <summary>
        /// 实例化ORM异常
        /// </summary>
        /// <param name="Msg">异常消息</param>
        /// <param name="Args">异常消息参数</param>
        public OrmException(string Msg, params object[] Args)
            : base(Msg, Args) {

        }
        /// <summary>
        /// 实例化ORM异常
        /// </summary>
        /// <param name="ex">导致当前异常的异常</param>
        /// <param name="Msg">异常消息</param>
        /// <param name="Args">异常消息参数</param>
        public OrmException(Exception ex, string Msg, params object[] Args)
            : base(ex, Msg, Args) {

        }
        /// <summary>
        /// 实例化ORM异常
        /// </summary>
        /// <param name="method">产生此异常时的方法</param>
        /// <param name="Msg">异常消息</param>
        /// <param name="Args">异常消息参数</param>
        public OrmException(MethodBase method, string Msg, params object[] Args)
            : base(method, Msg, Args) {
        }
    }
}
