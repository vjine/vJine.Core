﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;

namespace vJine.Core.ORM {
    /// <summary>
    /// 数据库命令跟踪代理
    /// </summary>
    /// <param name="ID">DataManager的ID</param>
    /// <param name="Action">命令类别</param>
    /// <param name="Table">数据库表名</param>
    /// <param name="Cmd">执行的SQL语句</param>
    /// <param name="Params">参数集合</param>
    /// <returns>错误消息处理代理，如果留空则如有错误则抛出错误消息</returns>
    public delegate Exec<Exception> OrmTraceDelegate(int ID, string Action, string Table, string Cmd, DbParameterCollection Params);
}
