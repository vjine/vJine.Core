#vJine.Core
***
>vJine.Core是由C#语言编写的优秀的ORM项目，具有轻量级、易上手、可扩展等特点。示例项目参见[vJine.Core.QuickStart](http://git.oschina.net/vjine/vJine.Core.QuickStart "vJine.Core.QuickStart")   

##授权：
[MPL2.0](https://www.mozilla.org/MPL/2.0/ "")  

##支持的数据库  
SQLite, ACCESS, MySQL, Microsoft SQL Server, PostgreSQL, Oracle  

##相关资源  

nuget：[vJine@Nuget.Org](https://www.nuget.org/packages?q=vjine "")  
API文档：[项目附件](http://git.oschina.net/vjine/vJine.Core/attach_files "")  
QQ群：115051701  

##示例代码
```cs
void IDUQ(string db) {
    DataManager dm = new DataManager(db);
    dm.OnTrace += (int ID, string Action, string Table, string Cmd, DbParameterCollection Params) => {
        System.Console.WriteLine("ID:{0},ACTION:{1},TABLE:{2},CMD:{3}", ID, Action, Table, Cmd);

        return null;
    };

           
    dm.Create<BizEntity>(true);
    string sql_create = dm.ToString();
            
    dm.I<BizEntity>(
        new BizEntity() { Name = "Name_1", Number = 1, ENUM = Enum_Type.E01 });
    dm.I<BizEntity>(
        new BizEntity() { Name = "Name_2", Number = 2, ENUM = Enum_Type.E02 });
    string sql_insert = dm.ToString();
            
    CollectionBase<BizEntity> container = new CollectionBase<BizEntity>();
    dm.Q<BizEntity>(container,
        BizEntity._.Name == "Name_1" | BizEntity._.Name == "Name_2");
    string sql_select = dm.ToString();
            
    dm.U<BizEntity>(
        BizEntity._.Name.EQ("Name_11") 
        BizEntity._.Name == "Name_1" );
    string sql_update = dm.ToString();
            
    dm.D<BizEntity>(BizEntity._.Name == "Name_11" | BizEntity._.Name == "Name_2");
    string sql_delete = dm.ToString();
            
    dm.Drop<BizEntity>();
    string sql_drop = dm.ToString();
}
```